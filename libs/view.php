<?php

class View{
    function __construct() {
        
    }

    function render($view, $title = 'NetLogical APP', $active = '', $data = []) {
        $this->body = $view.'.php';
        $this->title = $title;
        $this->active = $active;
        $this->d = $data;
        require "views/layout.php";
    }
    // flash messages ['message', 'type']
    function flash($type, $message) {
        if(!isset($_SESSION)) {
            session_start();
        }
        $_SESSION['message'] = ['type' => $type, 'message' => $message];
    }

}

?>