<?php
require_once 'controllers/errores.php';
class App {
    function __construct() {
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = explode('/', $url);

        // Si no ingreso nada despues de la direccion
        if(empty($url[0])) {
            $controllerFile = "controllers/home.php";
            require_once $controllerFile;
            $controller = new Home();
            $controller->loadModel('home');
            $controller->render();
            return false;
        }
        // La ruta del controlador dependiendo de la url
        $controllerFile = "controllers/$url[0].php";
        if(file_exists($controllerFile)) {
            // Importo el controlador
            require_once $controllerFile;
            // Instancio la clase del controlador
            $controller = new $url[0];
            $controller->loadModel($url[0]);

            $nparam = sizeof($url);

            // if(isset($url[1])) {
            //     $controller->{$url[1]}();
            // } else {
            //     $controller->render();
            // }
            if($nparam > 1) {
                if($nparam > 2){
                    $param = [];
                    for($i = 2; $i < $nparam; $i++) {
                        array_push($param, $url[$i]);
                    }
                    $controller->{$url[1]($param)};
                } else {
                    $controller->{$url[1]}();
                }
            } else {
                $controller->render();
            }
        } else {
            $controller = new Errores();
        }
    }
}

?>