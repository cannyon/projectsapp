<?php

class Controller {
    function __construct() {
        $this->view = new View();
    }

    function loadModel($model) {
        $model = ucfirst($model); // La primer letra en mayuscula
        $url = 'models/'.$model.'.php';
        if(file_exists($url)) {
            require_once $url;
            $this->model = new $model();
        } else {
            error_log("Controller::loadModel -> $model no encontrado");
        }
    }

    function redirect($route) {
        header('Location: '.constant('URL').'/'.$route);
    }
}

?>