-- CREATE DATABASE projectsapp;
-- use projectsapp;
CREATE TABLE users (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(35) NOT NULL,
    last_name VARCHAR(35) NOT NULL,
    username VARCHAR(35) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
    password CHAR(60) NOT NULL,
    career_id INTEGER,
    role ENUM('student', 'teacher', 'admin') DEFAULT 'student',
    dni VARCHAR(16),
    degree VARCHAR(128),
    exp_years TINYINT,
    email_public BOOLEAN DEFAULT FALSE,
    birthday DATE,
    profile_pic VARCHAR(128) DEFAULT 'default.png',
    registered_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE careers (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    resolution VARCHAR(16) NOT NULL UNIQUE
);

CREATE TABLE areas (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL
);

CREATE TABLE works (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    area_id INTEGER,
    creator_id INTEGER,
    date_start DATETIME NOT NULL,
    date_end DATETIME NOT NULL,
    budget DECIMAL(15, 2) DEFAULT 0.00,
    description TEXT,
    group_id INTEGER,
    state ENUM('onwork', 'stopped', 'cancelled', 'finished') DEFAULT 'onwork',
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (area_id) REFERENCES areas(id) ON DELETE SET NULL,
    FOREIGN KEY (creator_id) REFERENCES users(id) ON DELETE SET NULL,
    FOREIGN KEY (group_id) REFERENCES groups(id) ON DELETE SET NULL
);

CREATE TABLE uploads (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    filename VARCHAR(128) NOT NULL,
    url VARCHAR(128) NOT NULL,
    work_id INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    date_uploaded TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (work_id) REFERENCES works(id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE SET NULL
);

CREATE TABLE groups (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    creator_id INTEGER,
    area_id INTEGER,
    working_id INTEGER,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (creator_id) REFERENCES users(id) ON DELETE SET NULL,
    FOREIGN KEY (area_id) REFERENCES areas(id) ON DELETE SET NULL,
    FOREIGN KEY (working_id) REFERENCES works(id) ON DELETE SET NULL
);

CREATE TABLE members (
    group_id INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    status ENUM('user_sent', 'group_sent', 'member'),
    role ENUM('normal', 'admin'),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (group_id) REFERENCES groups(id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);

ALTER TABLE users ADD FOREIGN KEY (career_id) REFERENCES careers(id) ON DELETE SET NULL;

