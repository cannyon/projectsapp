const slcArea = document.getElementById('area');
const slcGroup = document.getElementById('group');
const url = slcArea.dataset.url;
const settedGroupId = slcGroup.dataset.group_id;

async function updateGroupsOptions() {
    const body = new FormData();
    const area_id = slcArea.options[slcArea.selectedIndex].value;
    body.append('area_id', area_id);
    await fetch(`${url}/groups/filter_groups_by_area`, {
        method: 'POST',
        body: body
    }).then(res => res.json())
      .then(groups => {
          while(slcGroup.childElementCount > 1) {
              slcGroup.removeChild(slcGroup.lastChild);
          }
          groups.forEach(group => {
            let option = document.createElement('option');
            option.value = group['id'];
            option.textContent = group['name'];
            if(group['id'] == settedGroupId) {
                option.setAttribute('selected', 'selected');
            }
            slcGroup.appendChild(option);
          })
          
          
    })
}
updateGroupsOptions();
slcArea.addEventListener('change', updateGroupsOptions);