// Le paso la constante de la URL desde PHP a JavaScript
const sctGroup = document.getElementById('group');
const url = sctGroup.dataset.url;
const group_id = sctGroup.dataset.group_id;
const btnSendRequest = document.getElementById('btnSendRequest');
const btnCancelRequest = document.getElementById('btnCancelRequest');
const btnLeave = document.getElementById('btnLeave');
const receivedRequestsList = document.getElementById('receivedRequestsList');
const freeUsersList = document.getElementById('freeUsersList');
const myRequests = document.getElementById('myRequests');


// Solicitudes recibidas de los usuarios
if(receivedRequestsList) {
    receivedRequestsList.addEventListener('click', async e => {
        // Aceptar solicitud
        if(e.target.id == 'btnAcceptRequest') {
            const body = new FormData();
            const user_id = e.target.parentNode.dataset.user_id;
            body.append('user_id', user_id);
            body.append('group_id', group_id);
            await fetch(`${url}/members/update_request`, {
                method: 'POST',
                body: body
            }).then(res => {
                // e.target.parentNode.parentNode.removeChild(e.parentNode);
                location.reload();
            })
        }
        // Rechazar solicitud
        if(e.target.id == 'btnRejectRequest') {
            const body = new FormData();
            const user_id = e.target.parentNode.dataset.user_id;
            body.append('user_id', user_id);
            body.append('group_id', group_id);
            await fetch(`${url}/members/delete_request`, {
                method: 'POST',
                body: body
            }).then(res => {
                e.target.parentNode.parentNode.removeChild(e.target.parentNode);
            })
        }
    });
}

if(freeUsersList) {
    freeUsersList.addEventListener('click', async e => {
        if(e.target.id == 'btnInvite') {
            const user_id = e.target.parentNode.dataset.user_id;
            const body = new FormData();
            body.append('group_id', group_id);
            body.append('user_id', user_id);
            await fetch(`${url}/members/group_send_request`, {
                method: 'POST',
                body: body
            }).then(res => {
                if(res.ok) {
                    let parent = e.target.parentNode;
                    parent.removeChild(e.target);
                    let btnCancel = document.createElement('button');
                    btnCancel.className = 'btn';
                    btnCancel.textContent = 'Cancelar solicitud';
                    btnCancel.id = 'btnCancel';
                    parent.appendChild(btnCancel);
                }
            });
        }
        if(e.target.id == 'btnCancel') {
            const user_id = e.target.parentNode.dataset.user_id;
            const body = new FormData();
            body.append('group_id', group_id);
            body.append('user_id', user_id);
            await fetch(`${url}/members/delete_request`, {
                method: 'POST',
                body: body
            }).then(res => {
                if(res.ok) {
                    let parent = e.target.parentNode;
                    parent.removeChild(e.target);
                    let btnInvite = document.createElement('button');
                    btnInvite.className = 'btn';
                    btnInvite.textContent = 'Enviar solicitud';
                    btnInvite.id = 'btnInvite';
                    parent.appendChild(btnInvite);
                }
            });
        }
    })
}
// El usuario puede aceptar o rechazar la solicitud del grupo
if(btnSendRequest) {
    btnSendRequest.addEventListener('click', sendRequest);
}
if(btnCancelRequest) {
    btnCancelRequest.addEventListener('click', deleteRequest);
}
if(btnLeave) {
    btnLeave.addEventListener('click', async e => {
        const body = new FormData();
        body.append('leave', 'true');
        body.append('group_id', group_id);
        await fetch(`${url}/members/delete_request`, {
            method: 'POST',
            body: body
        }).then(res => {
            if(res.ok) {
                window.location.reload();
            }
        })
    });
}
async function sendRequest(e) {
    const body = new FormData();
    body.append('group_id', group_id);
    await fetch(`${url}/members/user_send_request`, {
        method: 'POST',
        body: body
    }).then(res => {
        if(res.ok) {
            let parent = e.target.parentNode;
            parent.removeChild(e.target);
            let btnCancelRequest = document.createElement('button');
            btnCancelRequest.id = 'btnCancelRequest';
            btnCancelRequest.className = 'btn';
            btnCancelRequest.dataset.url = url;
            btnCancelRequest.dataset.group_id = group_id;
            btnCancelRequest.textContent = 'Cancelar solicitud';
            btnCancelRequest.addEventListener('click', deleteRequest);
            parent.appendChild(btnCancelRequest);
        }
    })
}
async function deleteRequest(e) {
    const body = new FormData();
    body.append('group_id', group_id);
    body.append('action', 'user_reject');
    await fetch(`${url}/members/delete_request`, {
        method: 'POST',
        body: body
    }).then(res => {
        if(res.ok) {
            let parent = e.target.parentNode;
            parent.removeChild(e.target);
            let btnSendRequestToGroup = document.createElement('button');
            btnSendRequestToGroup.id = 'btnSendRequest';
            btnSendRequestToGroup.setAttribute('type', 'button');
            btnSendRequestToGroup.className = 'btn';
            btnSendRequestToGroup.dataset.group_id = group_id;
            btnSendRequestToGroup.dataset.url = url;
            btnSendRequestToGroup.textContent = 'Enviar solicitud';
            btnSendRequestToGroup.addEventListener('click', sendRequest);
            parent.appendChild(btnSendRequestToGroup);
        }
    })
}

if(myRequests) {
    myRequests.addEventListener('click', async e => {
        if(e.target.id == 'btnAcceptRequest') {
            const body = new FormData();
            body.append('group_id', group_id);
            body.append('action', 'user_accept');
            await fetch(`${url}/members/update_request`, {
                method: 'POST',
                body: body
            }).then(res => {
                if(res.ok) {
                    window.scrollTo(0, 0);
                    window.location.reload();
                }
            })
        } else if (e.target.id == 'btnRejectRequest') {
            const body = new FormData();
            body.append('group_id', group_id);
            body.append('action', 'user_reject');
            await fetch(`${url}/members/delete_request`, {
                method: 'POST',
                body: body
            }).then(res => {
                if(res.ok) {
                    let parent = e.target.parentNode.parentNode;
                    parent.removeChild(e.target.parentNode);
                    let btnSendRequestToGroup = document.createElement('button');
                    btnSendRequestToGroup.id = 'btnSendRequest';
                    btnSendRequestToGroup.setAttribute('type', 'button');
                    btnSendRequestToGroup.className = 'btn';
                    btnSendRequestToGroup.dataset.group_id = group_id;
                    btnSendRequestToGroup.dataset.url = url;
                    btnSendRequestToGroup.textContent = 'Enviar solicitud';
                    btnSendRequestToGroup.addEventListener('click', sendRequest);
                    parent.appendChild(btnSendRequestToGroup);
                }
            })
        }
    });
}