const monthSelect = document.getElementById("monthSelect");
const daySelect = document.getElementById("daySelect");
const yearBdSelect = document.getElementById("yearSelect");


if (monthSelect && yearBdSelect) {
  let isLeapYear = false;
  let monthDays = 0;
  function changeDays() {
    let monthIndex = Number(
      monthSelect.options[monthSelect.selectedIndex].value
    );
    switch (monthIndex) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 9:
      case 12:
        monthDays = 31;
        break;
      case 4:
      case 6:
      case 10:
      case 11:
        monthDays = 30;
        break;
      case 2:
        if (isLeapYear) {
          monthDays = 29;
        } else {
          monthDays = 28;
        }
        break;
      default:
        monthDays = 31;
        break;
    }
    if (daySelect.childElementCount != monthDays + 1) {
      let previousIndex = daySelect.selectedIndex;
      while (daySelect.childElementCount > 1) {
        daySelect.removeChild(daySelect.lastChild);
      }
      for (let i = 1; i <= monthDays; i++) {
        let dayHtml = document.createElement("option");
        dayHtml.value = i;
        dayHtml.text = i;
        daySelect.appendChild(dayHtml);
      }
      if (previousIndex < monthDays + 1) {
        daySelect.selectedIndex = previousIndex;
      } else {
        daySelect.selectedIndex = 0;
      }
    }
  }
  monthSelect.addEventListener("change", changeDays);

  const today = new Date().getFullYear();
  for (let i = today; i >= 1900; i--) {
    let yearHtml = document.createElement("option");
    yearHtml.value = i;
    yearHtml.text = i;
    yearBdSelect.appendChild(yearHtml);
  }

  yearBdSelect.addEventListener("change", (e) => {
    let yearSelected = Number(e.target.value);
    if (
      (yearSelected % 4 == 0 && yearSelected % 100 != 0) ||
      yearSelected % 400 == 0
    ) {
      isLeapYear = true;
    } else {
      isLeapYear = false;
    }
    changeDays();
  });
}

// Functions to delete profile picture
let btnDeleteProfilePic = document.getElementById("btnDeleteProfilePic");
const urlHolder = document.getElementById("urlHolder");
const url = urlHolder.dataset.url;
const profilePicFile = document.getElementById("profilePicFile");
const profilePicFileCopy = profilePicFile.cloneNode(true);
const inputDeleteProfilePic = document.getElementById('deleted_picture');

function previewImage(e) {
  if (e.target.files[0]) {
    inputDeleteProfilePic.value = '0';
    let reader = new FileReader();
    reader.onload = (event) => {
      profile_pic.style.backgroundImage = `url('${event.target.result}')`;
    };
    reader.readAsDataURL(e.target.files[0]);
    if(!btnDeleteProfilePic) {
      createDeleteButton();
    }
  }
}

function createDeleteButton() {
  btnDeleteProfilePic = document.createElement("button");
  btnDeleteProfilePic.dataset.id = "btnDeleteProfilePic";
  btnDeleteProfilePic.type = "button";
  btnDeleteProfilePic.className = "btn";
  btnDeleteProfilePic.innerHTML = `Eliminar foto <span class="material-icons">delete</span>`;
  btnDeleteProfilePic.addEventListener("click", deleteProfilePic);
  const divProfilePic = document.getElementById("divProfilePic");
  divProfilePic.appendChild(btnDeleteProfilePic);
}

if (profilePicFile) {
  let profile_pic = document.getElementById("profile_pic");
  profilePicFile.addEventListener("change", previewImage);
}

async function deleteProfilePic(e) {
  inputDeleteProfilePic.value = '1';
  const parent = e.target.parentNode;
  profile_pic.style.backgroundImage = `url('${url}/public/img/pics/default.png')`;
  btnDeleteProfilePic = null;
  parent.removeChild(e.target);
  let inputCopy = profilePicFileCopy.cloneNode(true);
  inputCopy.addEventListener("change", previewImage);
  let inputPicture = document.getElementById("profilePicFile");
  inputPicture.parentNode.replaceChild(inputCopy, inputPicture);
}

if (btnDeleteProfilePic) {
  let profile_pic = document.getElementById("profile_pic");
  btnDeleteProfilePic.addEventListener("click", deleteProfilePic);
}
