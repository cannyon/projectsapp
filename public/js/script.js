// Navbar Hamburgermenu
const hamburgerToggle = document.getElementById('nav-toggle');
const hamburgerMenu = document.getElementById('nav-menu');
const btnProfile = document.getElementById('btnProfile');
const profileMenu = document.getElementById('profileMenu');
const alerts = document.querySelectorAll('.alert');

if (hamburgerToggle && hamburgerMenu) {
    hamburgerToggle.addEventListener('click', () => {
        hamburgerMenu.classList.toggle('nav__menu--show');
        hamburgerToggle.firstElementChild.innerHTML = hamburgerMenu.classList.contains('nav__menu--show') ? '&#10005;' : '&#9776;';
    });
}

if (btnProfile && profileMenu) {
  btnProfile.addEventListener('click', () => {
    profileMenu.classList.toggle('profile__menu--show');
  })
}

if (alerts) {
  alerts.forEach(alert => {
    alert.querySelector('button').addEventListener('click', function () {
      this.parentElement.remove();
    })
  })
}