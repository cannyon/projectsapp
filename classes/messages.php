<?php

class Messages {
    const ERROR_DEFAULT = 'Hubo un error';
    const ERROR_EMPTY_FIELDS = 'LLene todos los campos';
    const ERROR_AREAS_NAME_USED = 'Ya existe un área con este nombre';
    const ERROR_AREAS_NOT_EXISTS = 'No existe ese área';
    const ERROR_CAREERS_NOT_EXISTS = 'No existe la carrera';
    const ERROR_GROUPS_NOT_EXISTS = 'Grupo no existe';
    const ERROR_GROUPS_UPDATED = 'No se pudo actualizar el grupo';
    const ERROR_MEMBERS_REQUEST_SENT = 'No se pudo enviar la solicitud';
    const ERROR_USERS_NOT_EXISTS = 'El usuario no existe';
    const ERROR_USERS_UPDATED = 'No se ha podido actualizar el usuario';
    const ERROR_UNAUTHORIZED = 'No tienes acceso';
    const ERROR_WORKS_FILE = 'No se pudo subir el archivo';
    const ERROR_WORKS_NOT_EXISTS = 'No existe el trabajo';
    const ERROR_WORKS_UPDATED = 'No se pudo actualizar el trabajo';


    const SUCCESS_AREAS_CREATED = 'Área creada exitosamente';
    const SUCCESS_AREAS_UPDATED = 'Área actualizada exitosamente';
    const SUCCESS_AREAS_DELETED = 'Área eliminada exitosamente';
    const SUCCESS_USERS_CREATED = 'Te has registrado';
    const SUCCESS_USERS_UPDATED = 'Usuario actualizado';
    const SUCCESS_USERS_LOGIN = 'Has iniciado sesión';
    const SUCCESS_USERS_LOGGEDOUT = 'Has cerrado sesión';
    const SUCCESS_CAREERS_CREATED = 'Carrera creada exitosamente';
    const SUCCESS_CAREERS_UPDATED = 'Carrera actualzida exitosamente';
    const SUCCESS_CAREERS_DELETED = 'Carrera eliminada exitosamente';
    const SUCCESS_GROUPS_CREATED = 'Grupo creado exitosamente';
    const SUCCESS_GROUPS_UPDATED = 'Grupo actualizado exitosamente';
    const SUCCESS_GROUPS_DELETED = 'Grupo eliminado exitosamente';
    const SUCCESS_MEMBERS_REQUEST_SENT = 'Solicitud enviada';
    const SUCCESS_MEMBERS_ENTERED = 'Ingresaste al grupo';
    const SUCCESS_MEMBERS_LEFT = 'Abandonaste el grupo';
    const SUCCESS_WORKS_CREATED = 'Trabajo creado exitosamente';
    const SUCCESS_WORKS_FILE = 'Se subió el archivo';
    const SUCCESS_WORKS_UPDATED = 'Trabajo actualizado exitosamente';
    const SUCCESS_WORKS_DELETED = 'Trabajo eliminado exitosamente';
}
?>