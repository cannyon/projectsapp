<?php
require_once 'classes/session.php';
require_once 'models/User.php';

class SessionController extends Controller{
    private $session, $user, $sites;

    public function __construct() {
        parent::__construct();
        $this->init();
    }

    public function init() {
        $this->session = new Session();
        $json = $this->get_JSON_file_config();
        $this->sites = $json['sites'];
        $this->defaultSites = $json['default-sites'];

        $this->validate_session();
    }

    function exists_session() {
        if(!$this->session->exists()) return false;
        if($this->session->get_current_user() == NULL) return false;

        $userid = $this->session->get_current_user();
        return $userid ? true : false;
    }

    function get_user_session_data() {
        $id = $this->session->get_current_user();

        $this->user = new User();
        $this->user->get($id);
        return $this->user;
    }

    function initialize($user) {
        $this->session->set_current_user($user->get_id());
        $this->redirect($this->defaultSites[$user->get_role()]);

    }

    function logout() {
        $this->session->close_session();
        $this->view->flash('success', Messages::SUCCESS_USERS_LOGGEDOUT);
        $this->redirect('login');
    }

    function validate_session() {
        // si existe la sesion
        if($this->exists_session()) {
            $role = $this->get_user_session_data()->get_role();
            error_log('SessionController:: validate_session -> existe sesion. rol '.$role);
            
            // Si no esta autorizado por el rol, lo redirige
            if(!$this->is_authorized($role)) {
                error_log('SessionController:: validate_session -> no esta autorizado');
                $this->redirect_default_site_by_role($role);
            }
            
        } else {
            // No existe la sesion, verifica si es publica
            if(!$this->is_public()) {
                header('Location: '.constant('URL'));
            }
        }
    }

    function redirect_default_site_by_role($role) {
        $url = '';
        $index = array_search($role, array_column($this->sites, 'role'));
        if ($index != NULL) {
            $url = '/'.$this->sites[$index]['site'];
        }
        error_log($url);
        header('Location: '.constant('URL').$url);
    }

    function is_public() {
        $currentURL = $this->get_current_page();
        $currentURL = preg_replace("/\?.*/", "", $currentURL);
        // Busca si el acceso de la pagina es publico o privado
        $index = array_search($currentURL, array_column($this->sites, 'site'));
        if ($this->sites[$index]['access'] == 'public') {
            error_log('sessionController::is_public -> es publica: '.$currentURL);
            return true;
        }
        return false;
    }

    function is_authorized($role) {
        error_log('SessionController::is_authorized() -> '.$role);
        $currentURL = $this->get_current_page();
        $currentURL = preg_replace("/\?.*/", "", $currentURL);
        $index = array_search($currentURL, array_column($this->sites, 'site'));
        if ($this->sites[$index]['role'] == $role || $this->sites[$index]['role'] == '') {
            error_log('SessionContrller::is_authorized() -> El rol de la pagina '.$currentURL.' es '.$this->sites[$index]['role']);
            return true;
        } else {
            return false;
        }
    }

    function get_current_page() {
        $actualLink = trim("$_SERVER[REQUEST_URI]");
        $url = explode('/', $actualLink);
        if(sizeof($url) == 4) {
            return $url[2].'/'.$url[3];
        }
        error_log($url[2]);
        return $url[2];
    }

    function get_JSON_file_config() {
        $string = file_get_contents('config/access.json');
        $json = json_decode($string, true);
        return $json;
    }
}

?>