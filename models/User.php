<?php
require_once 'models/Career.php';
class User extends Model{
    private $id, $first_name, $last_name, $username, $email, $password, $role, $profile_pic, $career_id, $dni, $degree, 
            $exp_years, $email_public, $birthday, $registered_at;

    function __construct() {
        parent::__construct();
    }

    function fill_variables($data) {
        // $this->id = $data["id"];
        // $this->first_name = $data["first_name"];
        // $this->last_name = $data["last_name"];
        // $this->username = $data["username"];
        // $this->email = $data["email"];
        // $this->password = $data["password"];
        // $this->role = $data["role"];
        // $this->profile_pic = $data['profile_pic'];
        // $this->career_id = $data["career_id"];
        // $this->dni = $data["dni"];
        // $this->degree = $data["degree"];
        // $this->exp_years = $data["exp_years"];
        // $this->email_public = $data["email_public"];
        // $this->birthday = $data["birthday"];
        // $this->registered_at = $data["registered_at"];

        foreach($data as $key => $value) {
            $this->$key = $value;
        }
    }

    function get($id) {
        try{
            $query = $this->db->connect()->prepare('SELECT * FROM users WHERE id = ?');
            $query->execute([$id]);
            $user = $query->fetch();
            $this->fill_variables($user);
            return $this;
        } catch(PDOException $e) {
            return false;
        }
    }

    function get_all() {
        $students = [];
        $teachers = [];
        $admins = [];
        try {
            $queryStr = 'SELECT u.id, first_name, last_name, career_id, c.name AS career_name, role, profile_pic
                         FROM users AS u
                         LEFT JOIN careers AS c ON u.career_id = c.id';
            $query = $this->db->connect()->query($queryStr);
            while($r = $query->fetch()) {
                $user = new User();
                $user->id = $r['id'];
                $user->first_name = $r['first_name'];
                $user->last_name = $r['last_name'];
                $user->career_id = $r['career_id'];
                $user->career_name = $r['career_name'];
                $user->role = $r['role'];
                $user->profile_pic = $r['profile_pic'];
                if($user->role == 'student') {
                    array_push($students, $user);
                } else if($user->role == 'teacher'){
                    array_push($teachers, $user);
                } else {
                    array_push($admins, $user);
                }
            }
            return ['students' => $students, 'teachers' => $teachers, 'admins' => $admins];
        } catch (PDOException $e) {
            return NULL;
        }
    }
    // Guardar usuario en la DB
    function save() {
        try {
            $query_string = 'INSERT INTO users (first_name, last_name, username, email, password, birthday) VALUES
                (:first_name, :last_name, :username, :email, :password, :birthday)';
            $query = $this->db->connect()->prepare($query_string);
            $query->execute([
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
                'username' => $this->username,
                'email' => $this->email,
                'password' => $this->password,
                'birthday' => $this->birthday
            ]);
            return true;
        } catch(PDOException $e) {
            return false;
        }
    }

    function update() {
        try{
            $queryStr = 'UPDATE users
                         SET
                          first_name = :first_name,
                          last_name = :last_name,
                          email_public = :email_public,
                          profile_pic = :profile_pic,
                          dni = :dni,
                          exp_years = :exp_years,
                          degree = :degree
                         WHERE id = :id';
            $query = $this->db->connect()->prepare($queryStr);
            $query->execute([
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
                'id' => $this->id,
                'email_public' => $this->email_public,
                'profile_pic' => $this->profile_pic,
                'dni' => $this->dni,
                'exp_years' => $this->exp_years,
                'degree' => $this->degree
            ]);
            return true;
        }catch(PDOException $e) {
            error_log('User::update -> Error: '.$e->getMessage());
            return false;
        }
    }

    function update_profile_pic() {
        try{
            $queryStr = 'UPDATE users SET profile_pic = :profile_pic WHERE id = :id';
            $query = $this->db->connect()->prepare($queryStr);
            $query->execute([
                'id' => $this->id,
                'profile_pic' => $this->profile_pic
            ]);
            return true;
        }catch(PDOException $e) {
            return false;
        }
    }

    function update_dashboard() {
        try {
            $queryStr = 'UPDATE users SET role = :role, career_id = :career_id WHERE id = :id';
            $query = $this->db->connect()->prepare($queryStr);
            error_log($this->career_id);
            $query->execute([
                'role' => $this->role,
                'career_id' => $this->career_id,
                'id' => $this->id
            ]);
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    function delete($id) {
        try {
            $query = $this->db->connect()->prepare('DELETE FROM users WHERE id = ?');
            $query->execute([$id]);
            return true;
        } catch(PDOException $e) {
            return false;
        }
    }

    function exists($username, $email) {
        try {
            $query = $this->db->connect()->prepare('SELECT username, email FROM users WHERE username = :username OR email = :email');
            $query->execute(['username' => $username, 'email' => $email]);
            if ($query->rowCount() > 1) {
                // Si 2 usuarios tienen el usuario o correo TODO
                return true;
            } else if($query->rowCount() > 0) {
                // Si solo 1 usuario tiene el usuario y correo TODO
                return true;
            } else {
                // Si no existe
                return false;
            }
        }catch(PDOException $e) {
            return false;
        }
    }

    function exists_by_id($id) {
        try {
            $query = $this->db->connect()->prepare('SELECT id FROM users WHERE id = ?');
            $query->execute([$id]);
            // Si es mayor a 0 entonces existe
            return $query->rowCount() > 0;
        } catch(PDOException $e){
            return false;
        }
    }

    function get_profile_info($id) {
        try {
            $queryStr = 'SELECT users.id AS id, first_name, last_name, email, registered_at, birthday, role,
                careers.name AS career_name, degree, exp_years, dni, profile_pic, email_public
                FROM users
                LEFT JOIN careers ON users.career_id = careers.id
                WHERE users.id = :id';
            $query = $this->db->connect()->prepare($queryStr);
            $query->execute(['id' => $id]);
            $user = $query->fetch();
            $this->id = $user['id'];
            $this->first_name = $user['first_name'];
            $this->last_name = $user['last_name'];
            $this->email_public = $user['email_public'];
            $this->email = $user['email'];
            $this->registered_at = $user['registered_at'];
            $this->birthday = $user['birthday'];
            $this->role = $user['role'];
            $this->degree = $user['degree'];
            $this->exp_years = $user['exp_years'];
            $this->dni = $user['dni'];
            $this->set_profile_pic($user['profile_pic']);

            // Le asigno las variables del JOIN
            $this->career_name = $user['career_name'];
            return $this;
        } catch(PDOException $e){
            return false;
        }
    }

    function compare_passwords($password, $id) {
        try{
            $user = $this->get($id);
            return password_verify($password, $user->get_id());
        } catch(PDOException $e) {
            return false;
        }
    }

    function get_hashed_password($password) {
        return password_hash($password, PASSWORD_DEFAULT, ['cost' => 10]);
    }

    // Getters
    public function get_id() { return $this->id;}
    public function get_first_name() { return $this->first_name;}
    public function get_last_name() { return $this->last_name;}
    public function get_full_name() { return $this->first_name.' '.$this->last_name;}
    public function get_username() { return $this->username;}
    public function get_email() { return $this->email;}
    public function get_password() { return $this->password;}
    public function get_role() { return $this->role;}
    public function get_career_id() { return $this->career_id;}
    public function get_dni() { return $this->dni;}
    public function get_degree() { return $this->degree;}
    public function get_exp_years() { return $this->exp_years;}
    public function get_email_public() { return $this->email_public;}
    public function get_birthday() { return $this->birthday;}
    public function get_registered_at() { return $this->registered_at;}
    public function get_profile_pic() { return $this->profile_pic;}

    // Setters
    public function set_id($id) { $this->id = $id;}
    public function set_first_name($first_name) { $this->first_name = $first_name;}
    public function set_last_name($last_name) { $this->last_name = $last_name;}
    public function set_username($username) { $this->username = $username;}
    public function set_email($email) { $this->email = $email;}

    public function set_password($password, $hashed = true) {
        if ($hashed) {
            $this->password = $this->get_hashed_password($password);
        } else {
            $this->password = $password;
        }
    }
    public function set_role($role) { $this->role = $role;}
    public function set_career_id($career_id) { $this->career_id = $career_id;}
    public function set_dni($dni) { $this->dni = $dni;}
    public function set_degree($degree) { $this->degree = $degree;}
    public function set_exp_years($exp_years) { $this->exp_years = $exp_years;}
    public function set_email_public($email_public) { $this->email_public = $email_public;}
    public function set_birthday($birthday) { $this->birthday = $birthday;}
    public function set_registered_at($registered_at) { $this->registered_at = $registered_at;}
    public function set_profile_pic($profile_pic) { 
        $this->profile_pic = isset($profile_pic) ? $profile_pic : 'default.png';
    }
}
