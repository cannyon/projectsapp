<?php
require_once 'models/User.php';
class Member extends Model{
    private $group_id, $user_id, $status, $role;

    function __construct() {
        parent::__construct();
    }

    function fill_variables($data) {
        // $this->group_id = $data['group_id'];
        // $this->user_id = $data['user_id'];
        // $this->status = $data['status'];
        // $this->role = $data['role'];
        foreach($data as $key => $value) {
            $this->$key = $value;
        }
    }
    

    function get($group_id, $user_id) {
        try {
            $queryStr = 'SELECT * FROM members WHERE group_id = :group_id AND user_id = :user_id';
            $query = $this->db->connect()->prepare($queryStr);
            $query->execute(['group_id' => $group_id, 'user_id' => $user_id]);
            $membership = $query->fetch();
            $this->fill_variables($membership);
            return $this;
        } catch (PDOException $e) {
            return false;
        }
    }

    function get_members($group_id) {
        $members = [];
        try {
            $queryStr = 'SELECT id, first_name, last_name, profile_pic
                         FROM members JOIN users ON members.user_id = users.id
                         WHERE group_id = :group_id AND status = "member"';
            $query = $this->db->connect()->prepare($queryStr);
            $query->execute(['group_id' => $group_id]);
            while($row = $query->fetch()) {
                $member = new User();
                $member->set_id($row['id']);
                $member->set_first_name($row['first_name']);
                $member->set_last_name($row['last_name']);
                $member->set_profile_pic($row['profile_pic']);
                array_push($members, $member);
            }
            return $members;
        } catch (PDOException $e) {
            return NULL;
        }
    }

    function get_pending_requests($group_id) {
        $requests = [];
        try {
            $queryStr = 'SELECT group_id, user_id, status, first_name, last_name, profile_pic
                         FROM members m
                         JOIN users u ON m.user_id = u.id
                         WHERE group_id = :group_id AND status = "user_sent"';
            $query = $this->db->connect()->prepare($queryStr);
            $query->execute(['group_id' => $group_id]);
            while($row = $query->fetch()) {
                $request = new Member();
                $request->set_group_id($row['group_id']);
                $request->set_user_id($row['user_id']);
                $request->set_status($row['status']);
                $request->user_name = $row['first_name'].' '.$row['last_name'];
                $request->profile_pic = $row['profile_pic'];
                array_push($requests, $request);
            }
            return $requests;
        } catch (PDOException $e) {
            return NULL;
        }
    }

    function get_sent_requests($group_id) {
        $requests = [];
        try {
            $queryStr = 'SELECT group_id, user_id, status, first_name, last_name
                         FROM members m
                         JOIN users u ON m.user_id = u.id
                         WHERE group_id = :group_id AND status = "group_sent"';
            $query = $this->db->connect()->prepare($queryStr);
            $query->execute(['group_id' => $group_id]);
            while($row = $query->fetch()) {
                $request = new Member();
                $request->fill_variables($row);
                $request->user_name = $row['first_name'].' '.$row['last_name'];
                array_push($requests, $request);
            }
            return $requests;
        } catch (PDOException $e) {
            return NULL;
        }
    }

    function get_free_users($myId) {
        $freeUsers = [];
        try {
            // $queryStr = 'SELECT id, first_name, last_name, profile_pic
            //              FROM users LEFT JOIN members ON id = user_id
            //              WHERE id <> :myId AND (user_id IS NULL OR group_id <> :group_id)';
            $queryStr = 'SELECT id, first_name, last_name, profile_pic, group_id, status
                         FROM users LEFT JOIN members ON id = user_id
                         WHERE id != :myId AND (status = "group_sent" OR status IS NULL)';
            $query = $this->db->connect()->prepare($queryStr);
            $query->execute(['myId' => $myId]);
            while($row = $query->fetch()) {
                $freeUser = new User();
                // $freeUser->set_id($row['id']);
                // $freeUser->set_first_name($row['first_name']);
                // $freeUser->set_last_name($row['last_name']);
                // $freeUser->set_profile_pic($row['profile_pic']);
                $freeUser->fill_variables($row);
                array_push($freeUsers, $freeUser);
            }
            return $freeUsers;
        } catch (PDOException $e) {
            error_log('Member::get_free_users -> '.$e->getMessage());
            return NULL;
        }
    }

    function save() {
        try {
            $query = $this->db->connect()->prepare('INSERT INTO members (group_id, user_id, status)
                VALUES (:group_id, :user_id, :status)');
            $query->execute([
                'group_id' => $this->group_id,
                'user_id' => $this->user_id,
                'status' => $this->status
            ]);
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    function update() {
        try{
            $query = $this->db->connect()->prepare('UPDATE members SET status = :status, role = :role
                WHERE group_id = :group_id AND user_id = :user_id');
            $query->execute([
                'group_id' => $this->group_id,
                'user_id' => $this->user_id,
                'role' => $this->role,
                'status' => $this->status
            ]);
            return true;
        }catch(PDOException $e) {
            error_log('Member:: update-> Error: '.$e->getMessage());
            return false;
        }
    }

    function delete($group_id, $user_id) {
        try {
            $query = $this->db->connect()->prepare('DELETE FROM members WHERE group_id = :group_id
            AND user_id = :user_id');
            $query->execute(['group_id' => $group_id, 'user_id' => $user_id]);
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    function exists($group_id, $user_id) {
        try {
            $queryStr = 'SELECT group_id FROM members WHERE group_id = :group_id AND user_id = :user_id';
            $query = $this->db->connect()->prepare($queryStr);
            $query->execute(['group_id' => $group_id, 'user_id' => $user_id]);
            return $query->rowCount() > 0;
        } catch (PDOException $e) {
            return false;
        }
    }

    // Getters
    function get_user_id() { return $this->user_id; }
    function get_group_id() { return $this->group_id; }
    function get_role() { return $this->role; }
    function get_status() { return $this->status; }
    // Setters
    function set_user_id($user_id) { $this->user_id = $user_id; }
    function set_group_id($group_id) { $this->group_id = $group_id; }
    function set_role($role) { $this->role = $role; }
    function set_status($status) { $this->status = $status; }



}

?>