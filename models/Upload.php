<?php

class Upload extends Model
{
    private $id, $filename, $url, $work_id, $user_id, $date_uploaded;

    function __construct()
    {
        parent::__construct();
    }

    function fill_variables($data) {
        foreach($data as $key => $value) {
            $this->$key = $value;
        }
    }

    function work_has_upload($work_id) {
        try {
            $query = $this->db->connect()->prepare('SELECT id FROM uploads WHERE work_id = ?');
            $query->execute([$work_id]);
            return $query->rowCount() > 0;
        } catch (PDOException $e) {
            return false;
        }
    }

    function get_latest($work_id) {
        try {
            $query = $this->db->connect()->prepare('SELECT * FROM uploads WHERE work_id = ?');
            $query->execute([$work_id]);
            $upload = $query->fetch();
            $this->fill_variables($upload);
            return $this;
        } catch (PDOException $e) {
            return NULL;
        }
    }

    function get($id) {
        try {
            $query = $this->db->connect()->prepare('SELECT * FROM uploads WHERE id = ?');
            $query->execute([$id]);
            $upload = $query->fetch();
            $this->fill_variables($upload);
            return $this;
        } catch (PDOException $e) {
            return NULL;
        }
    }

    function save() {
        try {
            $queryStr = 'INSERT INTO uploads 
                            (filename, url, work_id, user_id)
                         VALUES
                            (:filename, :url, :work_id, :user_id)';
            $query = $this->db->connect()->prepare($queryStr);
            $query->execute([
                'filename' => $this->filename,
                'url' => $this->url,
                'work_id' => $this->work_id,
                'user_id' => $this->user_id
            ]);
            return true;
        } catch (PDOException $e) {
            error_log('Upload::save -> Error: '.$e->getMessage());
            return false;
        }
    }

    function delete($id) {
        try {
            $query = $this->db->connect()->prepare('DELETE FROM uploads WHERE id = ?');
            $query->execute([$id]);
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    

    public function get_id() { return $this->id;}
    public function get_filename(){ return $this->filename;}
    public function get_url(){ return $this->url;}
    public function get_work_id(){ return $this->work_id;}
    public function get_user_id(){ return $this->user_id;}
    public function get_date_uploaded(){ return $this->date_uploaded;}

    public function set_id($id) { $this->id = $id;}
    public function set_filename($filename) { $this->filename = $filename;}
    public function set_url($url) { $this->url = $url;}
    public function set_work_id($work_id) { $this->work_id = $work_id;}
    public function set_user_id($user_id) { $this->user_id = $user_id;}
    public function set_date_uploaded($date_uploaded) { $this->date_uploaded = $date_uploaded;}
}
