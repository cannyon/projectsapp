<?php
require_once 'models/Group.php';
class Work extends Model{
    private $id, $name, $creator_id, $date_start, $date_end, $budget, $area_id, $description,
        $group_id, $state, $created_at;
    function __construct() {
        parent::__construct();
    }
    

    public function fill_variables($data) {
        foreach($data as $key => $value) {
            $this->$key = $value;
        }
    }

    public function get($id) {
        try {
            $query = $this->db->connect()->prepare('SELECT * FROM works WHERE id = ?');
            $query->execute([$id]);
            $work = $query->fetch();
            $this->fill_variables($work);
            return $this;
        } catch (PDOException $e) {
            return false;
        }
    }

    public function get_all() {
        $works = [];
        try {
            $queryStr = 'SELECT w.id, w.name, area_id, state, created_at, a.name area_name 
                        FROM works w
                        LEFT JOIN areas a
                        ON w.area_id = a.id';
            $query = $this->db->connect()->query($queryStr);
            while($row = $query->fetch()) {
                $work = new Work();
                $work->id = $row['id'];
                $work->name = $row['name'];
                $work->area_id = $row['area_id'];
                $work->state = $row['state'];
                $work->created_at = $row['created_at'];
                $work->area_name = $row['area_name'];
                array_push($works, $work);
            }
            return $works;
            
        } catch (PDOException $e) {
            return NULL;
        }
    }

    public function get_all_created_by($id) {
        $works = [];
        try {
            $query = $this->db->connect()->prepare('SELECT * FROM works WHERE creator_id = ?');
            $query->execute([$id]);
            while($row = $query->fetch()) {
                $work = new Work();
                $work->fill_variables($row);
                array_push($works, $work);
            }
            return $works;
        } catch (PDOException $e) {
            return NULL;
        }
    }

    public function save() {
        try{
            $query = $this->db->connect()->prepare('INSERT INTO works
                (name, creator_id, date_start, date_end, budget, area_id, description)
                VALUES (:name, :creator_id, :date_start, :date_end, :budget, :area_id, :description)');
            $query->execute([
                'name' => $this->name,
                'creator_id' => $this->creator_id,
                'date_start' => $this->date_start,
                'date_end' => $this->date_end,
                'budget' => $this->budget,
                'area_id' => $this->area_id,
                'description' => $this->description
            ]);
            return true;
        } catch(PDOException $e) {
            return false;
        }
    }

    public function update() {
        try{
            $queryStr = 'UPDATE works
                         SET name = :name, budget = :budget, description = :description,
                            state = :state, area_id = :area_id, group_id = :group_id
                         WHERE id = :id';
            $query = $this->db->connect()->prepare($queryStr);
            $res = $query->execute([
                'name' => $this->name,
                'budget' => $this->budget,
                'description' => $this->description,
                'state' => $this->state,
                'area_id' => $this->area_id,
                'id' => $this->id,
                'group_id' => $this->group_id
            ]);
            return $res;
        }catch(PDOException $e) {
            return false;
        }
    }


    public function delete($id) {
        try {
            $query = $this->db->connect()->prepare('DELETE FROM users WHERE id = ?');
            $query->execute([$id]);
            return true;
        }catch(PDOException $e) {
            return false;
        }
    }

    public function exists($id) {
        try{
            $query = $this->db->connect()->prepare('SELECT id FROM works WHERE id = ?');
            $query->execute([$id]);
            return $query->rowCount() > 0;
        }catch(PDOException $e) {
            return false;
        }
    }

    public function get_work_info($id) {
        try{
            $queryStr = 'SELECT w.*, g.name group_name, first_name, last_name, a.name area_name
                         FROM works w
                         LEFT JOIN groups g ON w.group_id = g.id
                         LEFT JOIN users u ON w.creator_id = u.id
                         LEFT JOIN areas a ON w.area_id = a.id
                         WHERE w.id = :id';
            $query = $this->db->connect()->prepare($queryStr);
            $query->execute(['id' => $id]);
            $work = $query->fetch();
            $this->fill_variables($work);
            return $this;
        }catch(PDOException $e) {
            return false;
        }
    }

    // Getters
    public function get_id() {return $this->id;}
    public function get_name() {return $this->name;}
    public function get_creator_id() {return $this->creator_id;}
    public function get_date_start() {return $this->date_start;}
    public function get_date_end() {return $this->date_end;}
    public function get_budget() {return $this->budget;}
    public function get_area_id() {return $this->area_id;}
    public function get_description() {return $this->description;}
    public function get_group_id() {return $this->group_id;}
    public function get_state() {return $this->state;}
    public function get_created_at() {return $this->created_at;}
    // Setters
    public function set_id($id) {$this->id = $id;}
    public function set_name($name) {$this->name = $name;}
    public function set_creator_id($creator_id) {$this->creator_id = $creator_id;}
    public function set_date_start($date_start) {$this->date_start = $date_start;}
    public function set_date_end($date_end) {$this->date_end = $date_end;}
    public function set_budget($budget) {$this->budget = $budget;}
    public function set_area_id($area_id) {$this->area_id = $area_id;}
    public function set_description($description) {$this->description = $description;}
    public function set_group_id($group_id) {$this->group_id = $group_id;}
    public function set_state($state) {$this->state = $state;}
    public function set_created_at($created_at) {$this->created_at = $created_at;}

}

?>