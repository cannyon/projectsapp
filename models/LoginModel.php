<?php
require_once 'models/User.php';
class LoginModel extends Model{
    function __construct() {
        parent::__construct();
    }

    function login($username, $password) {
        try{
            $query = $this->db->connect()->prepare('SELECT * FROM users WHERE username = ?');
            $query->execute([$username]);

            if($query->rowCount() == 1) {
                $item = $query->fetch();
                $user = new User();
                $user->fill_variables($item);

                if(password_verify($password, $user->get_password())) {
                    return $user;
                } else {
                    return NULL;
                }
            }
        } catch(PDOException $e) {
            return NULL;
        }
    }
}

?>