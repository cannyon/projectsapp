<?php

class Area extends Model{
    private $id, $name;

    function __construct() {
        parent::__construct();
    }

    function get($id) {
        try {
            $query = $this->db->connect()->prepare('SELECT * FROM areas WHERE id = ?');
            $query->execute([$id]);
            $area = $query->fetch();
            $this->id = $area['id'];
            $this->name = $area['name'];
            return $this;
        } catch (PDOException $e) {
            return false;
        }
    }

    function get_all() {
        $areas = [];
        try {
            $query = $this->db->connect()->query('SELECT * FROM areas');
            while($row = $query->fetch()) {
                $area = new Area();
                $area->id = $row['id'];
                $area->name = $row['name'];
                array_push($areas, $area);
            }
            return $areas;
        } catch (PDOException $e) {
            return false;
        }
    }

    function save() {
        try {
            $query = $this->db->connect()->prepare('INSERT INTO areas (name) VALUES (:name)');
            $query->execute(['name' => $this->name]);
            if($query->rowCount()) return true;
            return false;
        } catch (PDOException $e) {
            return false;
        }
    }

    function update() {
        try {
            $query = $this->db->connect()->prepare('UPDATE areas SET name = :name WHERE id = :id');
            $query->execute(['name' => $this->name, 'id' => $this->id]);
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    function delete($id) {
        try{
            $query = $this->db->connect()->prepare('DELETE FROM areas WHERE id = ?');
            $query->execute([$id]);
            return true;
        }catch(PDOException $e) {
            return false;
        }
    }

    function exists($name) {
        try {
            $query = $this->db->connect()->prepare('SELECT id FROM areas WHERE name = ?');
            $query->execute([$name]);
            return $query->rowCount() > 0;
        } catch (PDOException $e) {
            return false;
        }
    }

    function exists_by_id($id) {
        try {
            $query = $this->db->connect()->prepare('SELECT id FROM areas WHERE id = ?');
            $query->execute([$id]);
            return $query->rowCount() > 0;
        } catch (PDOException $e) {
            return false;
        }
    }

    // Getters
    function get_id() {return $this->id;}
    function get_name() {return $this->name;}
    // Setters
    function set_id($id) {$this->id = $id;}
    function set_name($name) {$this->name = $name;}
}

?>