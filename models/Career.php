<?php

class Career extends Model{
    private $id, $name, $resolution;
    public function __construct() {
        parent::__construct();
    }
    // Getters
    public function get_id() {return $this->id;}
    public function get_name() {return $this->name;}
    public function get_resolution() {return $this->resolution;}
    // Setters
    public function set_id($id) {$this->id = $id;}
    public function set_name($name) {$this->name = $name;}
    public function set_resolution($resolution) {$this->resolution = $resolution;}
    

    public function fill_variables($data) {
        $this->id = $data['id'];
        $this->name = $data['name'];
        $this->resolution = $data['resolution'];
    }

    public function get($id) {
        try{
            $query = $this->db->connect()->prepare('SELECT * FROM careers WHERE id = :id');
            $query->execute(['id' => $id]);
            $career = $query->fetch();
            $this->fill_variables($career);
            return $this;
        } catch(PDOException $e) {
            return false;
        }
    }

    public function get_all() {
        $careers = [];
        try {
            $query = $this->db->connect()->query('SELECT * FROM careers');
            while ($row = $query->fetch()) {
                $career = new Career();
                $career->fill_variables($row);
                array_push($careers, $career);
            }
            return $careers;
        } catch (PDOException $e) {
            return false;
            echo $e->getMessage();
        }
        
    }

    public function save() {
        try{
            $query = $this->db->connect()->prepare('INSERT INTO careers (name, resolution) VALUES (:name, :resolution)');
            $query->execute(['name' => $this->name, 'resolution' => $this->resolution]);
        } catch(PDOException $e) {

        }
    }

    public function update() {
        try {
            $query = $this->db->connect()->prepare('UPDATE careers SET name = :name, resolution = :resolution');
            $query->execute(['name' => $this->name, 'resolution' => $this->resolution]);
            return true;
        } catch(PDOException $e) {
            return false;
        }
    }

    public function delete($id) {
        try{
            $query = $this->db->connect()->prepare('DELETE FROM careers WHERE id = ?');
            $query->execute([$id]);
            return true;
        } catch(PDOException $e) {
            return false;
        }
    }

    public function exists($resolution) {
        try {
            $query = $this->db->connect()->prepare('SELECT id FROM careers WHERE resolution = ?');
            $query->execute([$resolution]);
            return $query->rowCount() ? true : false;
        } catch (PDOException $e) {
            return false;
        }
    }

    public function exists_by_id($id) {
        try {
            $query = $this->db->connect()->prepare('SELECT id FROM careers WHERE id = ?');
            $query->execute([$id]);
            return $query->rowCount() ? true : false;
        } catch (PDOException $e) {
            return false;
        }
    }
}

?>