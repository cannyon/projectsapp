<?php
require_once 'models/User.php';
require_once 'models/Member.php';
class Group extends Model{
    private $id, $name, $creator_id, $area_id, $working_id, $created_at;
    function __construct() {
        parent::__construct();
    }
    
    public function fill_variables($data) {
        foreach($data as $key => $value) {
            $this->$key = $value;
        }
    }
    // Obtener un grupo individual
    public function get($id) {
        try{
            $query = $this->db->connect()->prepare('SELECT * FROM groups WHERE id = :id');
            $query->execute(['id' => $id]);
            $group = $query->fetch();
            $this->fill_variables($group);
            return $this;
        } catch(PDOException $e) {
            return false;
        }
    }

    public function get_all() {
        $groups = [];
        try {
            $queryStr = 'SELECT g.id, g.name, a.name area_name,
                            (SELECT COUNT(user_id)
                             FROM members
                             WHERE group_id = g.id AND status = "member") members
                         FROM groups g
                         LEFT JOIN areas a ON g.area_id = a.id
                         ';
            $query = $this->db->connect()->query($queryStr);
            while($row = $query->fetch()) {
                $group = new Group();
                $group->fill_variables($row);
                array_push($groups, $group);
            }
            return $groups;
        } catch (PDOException $e) {
            return NULL;
        }
    }

    public function get_all_info($id) {
        try {
            $queryStr = 'SELECT g.name, g.id, first_name, last_name, g.creator_id, 
                        area_id, a.name AS area_name, working_id, created_at
                        FROM groups g
                        LEFT JOIN users u
                        ON g.creator_id = g.id
                        LEFT JOIN areas a
                        ON g.area_id = a.id
                        WHERE g.id = :id';
            $query = $this->db->connect()->prepare($queryStr);
            $query->execute(['id' => $id]);
            $group = $query->fetch();
            $this->fill_variables($group);
            //asigno las variables
            $this->creator_name = $group['first_name'].' '.$group['last_name'];
            $this->area_name = $group['area_name'];
            return $this;
        } catch (PDOException $e) {
            error_log('Group::get_all_info -> '.$e->getMessage());
            return NULL;
        }
    }
    // Obtener todos los grupos de un area
    public function get_all_by_area($area_id) {
        error_log('Group:: get_all_by_area -> area_id: '.$area_id);
        try {
            $query = $this->db->connect()->prepare('SELECT id, name FROM groups WHERE area_id = ?');
            $query->execute([$area_id]);
            $groups = $query->fetchAll();
            return $groups;
        } catch (PDOException $e) {
            return false;
            echo $e->getMessage();
        }
    }

    function get_all_belong($user_id) {
        $groups = [];
        try {
            // $queryStr = 'SELECT id, name FROM groups JOIN members ON id = group_id WHERE user_id = :user_id';
            $queryStr = 'SELECT id, name FROM groups JOIN members ON id = group_id WHERE user_id = :user_id AND status = "member"';
            $query = $this->db->connect()->prepare($queryStr);
            $query->execute(['user_id' => $user_id]);
            while($row = $query->fetch()) {
                $group = new Group();
                $group->fill_variables($row);
                array_push($groups, $group);
            }
            return $groups;
        } catch (PDOException $e) {
            return NULL;
        }
    }

    function get_invitations($user_id) {
        $groups = [];
        try {
            $queryStr = 'SELECT id, name FROM groups JOIN members ON id = group_id WHERE user_id = :user_id AND status = "group_sent"';
            $query = $this->db->connect()->prepare($queryStr);
            $query->execute(['user_id' => $user_id]);
            while($row = $query->fetch()) {
                $group = new Group();
                $group->fill_variables($row);
                array_push($groups, $group);
            }
            return $groups;
        } catch (PDOException $e) {
            return NULL;
        }
    }

    public function get_all_created_by($id) {
        $groups = [];
        try {
            $query = $this->db->connect()->prepare('SELECT * FROM groups WHERE creator_id = ?');
            $query->execute([$id]);
            while($row = $query->fetch()) {
                $group = new Group();
                $group->fill_variables($row);
                array_push($groups, $group);
            }
            return $groups;
        } catch (PDOException $e) {
            return NULL;
        }
    }

    public function save() {
        try{
            $query = $this->db->connect()->prepare('INSERT INTO groups (name, creator_id, area_id)
                VALUES (:name, :creator_id, :area_id)');
            $query->execute([
                'name' => $this->name,
                'creator_id' => $this->creator_id,
                'area_id' => $this->area_id
            ]);
            if($query->rowCount()) return true;
            return false;
        }catch(PDOException $e) {
            return false;
        }
    }

    public function update() {
        try {
            $query = $this->db->connect()->prepare('UPDATE groups SET name = :name, area_id = :area_id 
                                                    WHERE id = :id');
            $query->execute(['name' => $this->name, 'id' => $this->id, 'area_id' => $this->area_id]);
            return true;
        } catch(PDOException $e) {
            return false;
        }
    }

    public function delete($id) {
        try {
            $query = $this->db->connect()->prepare('DELETE FROM groups WHERE id = ?');
            $query->execute([$id]);
            return true;
        }catch(PDOException $e) {
            return false;
        }
    }

    public function get_all_members($group_id) {
        $members = [];
        try {
            $queryStr = 'SELECT u.name, u.id
                        FROM members m
                        JOIN users u
                        ON m.user_id = u.id
                        JOIN groups g
                        ON m.group_id = g.id
                        WHERE g.id = :group_id';
            $query = $this->db->connect()->prepare($queryStr);
            $query->execute([
                'group_id' => $group_id
            ]);
            while($row = $query->fetch()) {
                $member = new Member();
                $member->fill_variables($row);
                array_push($members, $member);
            }
            return $members;
        } catch (PDOException $e) {
            return false;
        }
    }

    function exists($id) {
        try {
            $query = $this->db->connect()->prepare('SELECT id FROM groups WHERE id = ?');
            $query->execute([$id]);
            return $query->rowCount() > 0;
        } catch (PDOException $e) {
            return false;
        }
    }
    // Getters
    public function get_id() {return $this->id;}
    public function get_name() {return $this->name;}
    public function get_creator_id() {return $this->creator_id;}
    public function get_area_id() {return $this->area_id;}
    public function get_working_id() {return $this->working_id;}
    public function get_created_at() {return $this->created_at;}
    // Setters
    public function set_id($id) {$this->id = $id;}
    public function set_name($name) {$this->name = $name;}
    public function set_creator_id($creatorid) {$this->creator_id = $creatorid;}
    public function set_area_id($area_id) {
        $this->area_id = $area_id == 'null' ? NULL : $area_id;
    }
    public function set_working_id($workingid) {$this->working_id = $workingid;}
    public function set_created_at($created_at) {$this->created_at = $created_at;}
}

?>