<?php

class Institute extends SessionController {
    private $user;
    function __construct() {
        parent::__construct();
        if($this->exists_session()) {
            $this->view->user = $this->get_user_session_data();
        }
    }

    function render() {
        $this->view->render('home/institute', 'Instituto', 'institute');
    }
}

?>