<?php
require_once 'models/Career.php';
require_once 'models/Area.php';
require_once 'models/User.php';
class Dashboard extends SessionController{
    function __construct() {
        parent::__construct();
        $this->view->user = $this->get_user_session_data();
    }

    function render() {
        $this->view->render('dashboard/index', 'Dashboard', '');
    }

    function careers() {
        $career = new Career();
        // Modo editar
        if(isset($_GET['edit'])) {
            $id = $_GET['edit'];
            if($career->exists_by_id($id)) {
                $career = $career->get($id);
                $this->view->render('dashboard/careerEdit', 'Editar carrera', '', ['career' => $career]);
            } else {
                $this->view->flash('error', messages::ERROR_CAREERS_NOT_EXISTS);
                $this->redirect('dashboard/careers');
            }
        } else {
            // Modo ver
            $careers = $career->get_all();
            $this->view->render('dashboard/careerCreate', 'Carreras', '', ['careers' => $careers]);
        }
    }

    function areas() {
        $area = new Area();
        if(isset($_GET['edit'])) {
            // Si en la url esta /areas?edit=id
            $id = $_GET['edit'];
            if($area->exists_by_id($id)) {
                $area = $area->get($id);
                $this->view->render('dashboard/areaEdit', 'Editar área', '', ['area' => $area]);
            } else {
                $this->view->flash('error', messages::ERROR_AREAS_NOT_EXISTS);
                error_log('Dashboard:: areas -> no existe con esa id');
                $this->redirect('dashboard/areas');
            }

        } else {
            // Renderizo la pagina de crear areas y ver
            $areas = $area->get_all();
            $this->view->render('dashboard/areaCreate', 'Crear área', '', ['areas' => $areas]);
        }
    }

    // Pantalla de editar usuarios permisos
    function users() {
        if (isset($_GET['edit'])) {
            $id = $_GET['edit'];
            $userProfile = new User();
            $careers = new Career();
            $careers = $careers->get_all();
            if($userProfile->exists_by_id($id)) {
                $userProfile = $userProfile->get_profile_info($id);
                $this->view->render('dashboard/userEdit', 'Editar usuario '.$userProfile->get_full_name(), '', ['user_edit' => $userProfile, 'careers' => $careers]);
            } else {
                $this->view->flash('error', Messages::ERROR_USERS_NOT_EXISTS);
                $this->redirect('dashboard/users');
            }

        } else {
            $users = new User();
            $users = $users->get_all();
            $this->view->render('dashboard/users', 'Usuarios', '', ['users' => $users]);
        }
    }
    // Actualizar usuario con permiso nuevo y carrera
    function update_user() {
        if(isset($_POST['role']) && isset($_POST['career_id']) && isset($_POST['id'])) {
            $id = $_POST['id'];
            $role = $_POST['role'];
            $career_id = $_POST['career_id'];
            if (!is_int($career_id)) {
                $career_id = NULL;
            }
            $user = new User();
            $user->set_id($id);
            $user->set_role($role);
            $user->set_career_id($career_id);
            if($user->update_dashboard()) {
                $this->view->flash('success', Messages::SUCCESS_USERS_UPDATED);
            } else {
                $this->view->flash('error', Messages::ERROR_USERS_UPDATED);
            }
            $this->redirect('dashboard/users');
        }
    }

    function new_career() {
        if(isset($_POST['name']) && isset($_POST['resolution'])) {
            $name = $_POST['name'];
            $resolution = $_POST['resolution'];

            if($name == '' || empty($name) || $resolution == '' || empty($resolution)) {
                $this->redirect('dashboard');
            }
            $career = new Career();
            $career->set_name($name);
            $career->set_resolution($resolution);

            if($career->exists($resolution)) {
                $this->redirect('dashboard/careers');
            } else if($career->save()) {
                // Si se guardo exitosamente
                $this->redirect('dashboard/careers');
            } else {
                $this->redirect('dashboard/careers');
            }
        } else {
            $this->redirect('dashboard/careers');
        }
    }

    function update_career() {
        if(isset($_POST['name']) && isset($_POST['resolution']) && isset($_POST['id'])) {
            $id = $_POST['id'];
            $name = $_POST['name'];
            $resolution = $_POST['resolution'];
            if(is_int($id) || $name == '' || empty($name) || $resolution == '' || empty($resolution)) {
                $this->redirect('dashboard');
                return;
            }
            $career = new Career();
            $career->set_id($id);
            $career->set_name($name);
            $career->set_resolution($resolution);
            if($career->update()) {
                $this->view->flash('success', messages::SUCCESS_CAREERS_UPDATED);
                $this->redirect('dashboard/careers');
            }
        } else {
            $this->redirect('dashboard/careers');
        }
    }

    function delete_career() {
        if(isset($_POST['id'])) {
            $id = $_POST['id'];
            $career = new Career();
            $res = $career->delete($id);
            if($res) {
                $this->view->flash('success', messages::SUCCESS_CAREERS_DELETED);
                $this->redirect('dashboard/careers');
            } else {
                $this->view->flash('error', messages::ERROR_DEFAULT);
                $this->redirect('dashboard/careers');
            }
        }
    }

    // Areas
    function new_area() {
        error_log('Dashboard:: new_area -> Flag '.$_POST['name']);
        if(isset($_POST['name'])) {
            $name = $_POST['name'];
            if($name == '' || empty($name)) {
                $this->view->flash('error', messages::ERROR_EMPTY_FIELDS);
                $this->redirect('dashboard');
            }
            $area = new Area();
            $area->set_name($name);
            if($area->exists($name)) {
                $this->view->flash('error', messages::ERROR_AREAS_NAME_USED);
                $this->redirect('dashboard');
            } else if($area->save()) {
                $this->view->flash('success', messages::SUCCESS_AREAS_CREATED);
                $this->redirect('dashboard');
            } else {
                $this->view->flash('error', messages::ERROR_DEFAULT);
                $this->redirect('dashboard');
            }
        } else {
            $this->redirect('dashboard/areas');
        }
    }

    function update_area() {
        if(isset($_POST['name']) && isset($_POST['id'])) {
            $id = $_POST['id'];
            $name = $_POST['name'];
            if($name == '' || empty($name) || $id == '' || empty($id)) {
                $this->redirect('dashboard');
                return;
            }
            $area = new Area();
            $area->set_id($id);
            $area->set_name($name);
            if($area->update()) {
                $this->view->flash('success', messages::SUCCESS_AREAS_UPDATED);
                $this->redirect('dashboard/areas');
            }
        } else {
            $this->view->flash('error', messages::ERROR_DEFAULT);
            $this->redirect('dashboard/areas');
        }
    }

    function delete_area() {
        if(isset($_POST['id'])) {
            $id = $_POST['id'];
            $area = new Area();
            $res = $area->delete($id);
            if($res) {
                $this->view->flash('success', messages::SUCCESS_AREAS_DELETED);
                $this->redirect('dashboard/areas');
            } else {
                $this->view->flash('error', messages::ERROR_DEFAULT);
                $this->redirect('dashboard/areas');
            }
        }
    }
}

?>