<?php
require_once 'models/Career.php';
class Home extends SessionController{
    private $user;
    function __construct() {
        parent::__construct();
        if($this->exists_session()) {
            $this->view->user = $this->get_user_session_data();
        }
    }

    function render() {
        $careermodel = new Career();
        $this->view->render('home/index', 'Inicio', 'home');
    }

}
?>