<?php
require_once 'models/Work.php';
require_once 'models/Area.php';
require_once 'models/Upload.php';
class Researches extends SessionController{
    private $user;
    function __construct(){
        parent::__construct();
        // $this->loadModel('Work');
        if($this->exists_session()) {
            $this->view->user = $this->get_user_session_data();
            $this->user = $this->get_user_session_data();
        }
    }

    function render() {
        $works = new Work();
        $works = $works->get_all();
        $this->view->render('home/researches', 'Investigaciones', 'researches', ['works' => $works]);
    }


    // Ver trabajo individual /researches/works?id=n
    function works() {
        if(isset($_GET['id'])) {
            $id = $_GET['id'];
            $work = new Work();
            if($work->exists($id)) {
                // Pido la informacion de los joins de los works, areas, y careers
                $work = $work->get_work_info($id);
                $canEdit = false;
                $canEditUpload = false;
                if($this->user) {
                    $canEdit = $work->get_creator_id() == $this->user->get_id();
                    $canEditUpload = $work->get_creator_id() == $this->user->get_id();
                }
                $uploadM = new Upload();
                $hasUpload = $uploadM->work_has_upload($id);
                $upload = NULL;
                if($hasUpload) {
                    $upload = $uploadM->get_latest($id);
                }
                $this->view->render('researches/works', $work->get_name(), 'researches', 
                    [
                        'work' => $work,
                        'canEdit' => $canEdit,
                        'hasUpload' => $hasUpload,
                        'upload' => $upload,
                        'canEditUpload' =>$canEditUpload
                    ]
                );
            } else {
                $this->view->flash('error', messages::ERROR_WORKS_NOT_EXISTS);
                $this->redirect('researches');
            }
        } else {
            $this->redirect('researches');
        }
    }

    function create() {
        $areas = new Area();
        $areas = $areas->get_all();
        error_log(''.sizeof($areas));
        $this->view->render('researches/create', 'Crear un proyecto', 'researches', ['areas' => $areas]);
            
    }

    function is_it_mine($id) {
        return $id == $this->user->get_id();
    }

    function edit() {
        if(isset($_GET['id'])) {
            $id = $_GET['id'];
            $work = new Work();
            if($work->exists($id)) {
                $work = $work->get($id);
                if($this->is_it_mine($work->get_creator_id())) {
                    $areas = new Area();
                    $areas = $areas->get_all();
                    $this->view->render('researches/edit', 'Editar proyecto', 'researches', ['work' => $work, 'areas' => $areas]);
                } else {
                    $this->view->flash('error', Messages::ERROR_UNAUTHORIZED);
                    $this->redirect('researches/works?id='.$id);
                }
            } else {
                $this->view->flash('danger', Messages::ERROR_WORKS_NOT_EXISTS);
                $this->redirect('researches');
            }
        } else {
            $this->redirect('researches');
        }
    }

    function upload() {
        if(isset($_GET['id'])) {
            $id = $_GET['id'];
            $work = new Work();
            if($work->exists($id)) {
                $work = $work->get($id);
                if($this->is_it_mine($work->get_creator_id())) {
                    $uploadM = new Upload();
                    $upload = $uploadM->work_has_upload($id);
                    if($upload) {
                        $upload = $uploadM->get_latest($id);
                    }
                    $this->view->render('researches/upload', 'Editar proyecto', 'researches', ['work' => $work, 'upload' => $upload]);
                } else {
                    $this->view->flash('error', Messages::ERROR_UNAUTHORIZED);
                    $this->redirect('researches/works?id='.$id);
                }
            } else {
                $this->view->flash('danger', Messages::ERROR_WORKS_NOT_EXISTS);
                $this->redirect('researches');
            }
        }else {
            $this->redirect('researches');
        }
    }

    function new_research() {
        if(isset($_POST['name']) && isset($_POST['description']) && isset($_POST['date_start']) && isset($_POST['date_end'])) {
            $name = $_POST['name'];
            $description = $_POST['description'];
            $date_start = $_POST['date_start'];
            $date_end = $_POST['date_end'];
            $area_id = $_POST['area_id'];
            $budget = $_POST['budget'];

            if($name == '' || $description == '') {
                $this->view->flash(messages::ERROR_EMPTY_FIELDS);
                $this->redirect('researches/create');
            }
            $work = new Work();
            $work->set_name($name);
            $work->set_description($description);
            $work->set_date_start($date_start);
            $work->set_date_end($date_end);
            $work->set_area_id($area_id);
            $work->set_budget($budget);
            $work->set_creator_id($this->view->user->get_id());

            $res = $work->save();
            if($res) {
                $this->view->flash('success', messages::SUCCESS_WORKS_CREATED);
            } else {
                $this->view->flash('error', messages::ERROR_DEFAULT);
            }
            $this->redirect('researches');
        } else {
            $this->redirect('researches/create');
        }
    }

    function update_research() {
        if(isset($_POST['id']) && isset($_POST['name']) && isset($_POST['state'])) {
            $id = $_POST['id'];
            $name = $_POST['name'];
            $description = $_POST['description'];
            $date_start = $_POST['date_start'];
            $date_end = $_POST['date_end'];
            $area_id = !is_numeric($_POST['area_id'])? NULL : $_POST['area_id'];
            $budget = $_POST['budget'];
            $state = $_POST['state'];
            $group_id = !is_numeric($_POST['group_id'])? NULL : $_POST['group_id'];
            error_log('Researches::update_research -> group_id = '.$group_id);
            if(empty($name) || !is_int($id)) {
                $this->view->flash('error', Messages::ERROR_EMPTY_FIELDS);
                $this->redirect('researches');
            }

            $work = new Work();
            $work->set_id($id);
            $work->set_name($name);
            $work->set_description($description);
            $work->set_date_start($date_start);
            $work->set_date_end($date_end);
            $work->set_area_id($area_id);
            $work->set_budget($budget);
            $work->set_creator_id($this->view->user->get_id());
            $work->set_state($state);
            $work->set_group_id($group_id);
            $res = $work->update();
            if($res) {
                
                $this->view->flash('success', Messages::SUCCESS_WORKS_UPDATED);
            }else {
                $this->view->flash('error', Messages::ERROR_WORKS_UPDATED);
            }

        }
        $this->redirect('researches/works?id='.$id);
    }

    function upload_file() {
        if(isset($_POST['id']) && isset($_FILES['pdf'])) {
            $file = $_FILES['pdf'];
            $target_dir = 'public/doc/';
            $extension = explode('.', $file['name']);
            $filename = $extension[sizeof($extension) -2];
            $ext = $extension[sizeof($extension) - 1];
            $hash = md5(Date('Ymdgi').$filename).'.'.$ext;
            $target_file = $target_dir.$hash;
            if(move_uploaded_file($file['tmp_name'], $target_file)) {
                $upload = new Upload();
                $upload->set_filename($file['name']);
                $upload->set_url($hash);
                $upload->set_work_id($_POST['id']);
                $upload->set_user_id($this->user->get_id());
                $upload->save();
                $this->view->flash('success', Messages::SUCCESS_WORKS_FILE);
            } else {
                $this->view->flash('error', Messages::ERROR_WORKS_FILE);
            }
            return $this->redirect('researches/works?id='.$_POST['id']);
        }
        return $this->redirect('researches');
    }

}

?>