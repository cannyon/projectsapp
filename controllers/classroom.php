<?php
require_once 'models/Group.php';
require_once 'models/Work.php';
require_once 'models/Member.php';
class Classroom extends SessionController{
    private $user;
    function __construct() {
        parent::__construct();
        // Pagina solo accesible con sesion, no verifico
        if($this->exists_session()) {
            $this->user = $this->get_user_session_data();
            $this->view->user = $this->get_user_session_data();
        } else {
            exit();
        }
    }

    function render() {
        $groupsM = new Group();
        $myGroups = $groupsM->get_all_created_by($this->user->get_id());
        $groupsIBelongTo = $groupsM->get_all_belong($this->user->get_id());
        $myWorks = new Work();
        $myWorks = $myWorks->get_all_created_by($this->user->get_id());
        $this->view->render('classroom/index', 'Salón', 'classroom', 
            [
                'myGroups' => $myGroups,
                'myWorks' => $myWorks,
                'groupsIBelongTo' => $groupsIBelongTo
            ]
        );
    }


}

?>