<?php

class Login extends SessionController{
    function __construct() {
        parent::__construct();
    }

    function render() {
        $this->view->render('login/index', 'Iniciar sesión');
    }

    function loadModel($model) {
        $model = 'LoginModel';
        $url = 'models/'.$model.'.php';
        if(file_exists($url)) {
            require $url;
            $this->model = new $model();
        }
    }
    
    function authenticate() {
        if (isset($_POST['username']) && isset($_POST['password'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];

            // Si el usuario o contraseña estan vacios
            if($username == '' || empty($username) || $password == '' || empty($password)) {
                $this->view->flash('error', 'No se llenaron todos los campos');
                $this->redirect('');
            }
            $user = $this->model->login($username, $password);
            if($user != NULL) {
                // Si todo OK, inicia sesion :D
                $this->view->flash('success', 'Inició sesión');
                $this->initialize($user);
            } else {
                error_log('Error al loguearse');
                $this->redirect('');
            }
        } else {
            $this->redirect('login');
        }
    }

    function auto_login($username, $password) {
        $this->loadModel('LoginModel');
        $user = $this->model->login($username, $password);
        if($user != NULL) {
            // Si todo OK, inicia sesion :D
            $this->view->flash('success', 'Inició sesión');
            $this->initialize($user);
        } else {
            error_log('Error al loguearse');
            $this->redirect('');
        }

    }
}

?>