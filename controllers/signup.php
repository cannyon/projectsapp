<?php
require_once 'models/User.php';

class Signup extends SessionController {
    function __construct() {
        parent::__construct();
    }

    function render() {
        $this->view->render('login/signup', 'Registrarse');
    }

    function new_user() {
        if(isset($_POST['username']) && isset($_POST['password']) && isset($_POST['email']) && isset($_POST['first_name']) && isset($_POST['last_name'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $email = $_POST['email'];
            $first_name = $_POST['first_name'];
            $last_name = $_POST['last_name'];

            $year = $_POST['year'];
            $month = $_POST['month'];
            $day = $_POST['day'];
            
            $birthday = "$year-$month-$day";

            if($username == '' || empty($username) || $password == '' || empty($password)) {
                $this->redirect('');
            }
            $user = new User();
            $user->set_username($username);
            $user->set_password($password);
            $user->set_email($email);
            $user->set_first_name($first_name);
            $user->set_last_name($last_name);
            $user->set_birthday($birthday);

            if($user->exists($username, $email)) {
                // si ya existe usuario con ese nombre o correo
                $this->redirect('signup');
            } else if($user->save()) {
                // Si se registra exitosamente
                require_once 'login.php';
                $login = new Login();
                $login->auto_login($username, $password);
            } else {
                $this->redirect('signup');
            }
        } else {
            $this->redirect('signup');
        }
    }
}

?>