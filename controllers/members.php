<?php
// require_once 'models/Member.php';
require_once 'models/User.php';
require_once 'models/Group.php';

class Members extends SessionController{
    private $user;
    function __construct() {
        parent::__construct();
        $this->loadModel('Member');
        $this->user = $this->get_user_session_data();
    }

    function user_send_request() {
        if(isset($_POST['group_id'])) {
            $user_id = $this->user->get_id();
            $group_id = $_POST['group_id'];
            $status = 'user_sent';
            $group = new Group();
            if ($this->model->exists($group_id, $user_id) || !$group->exists($group_id)) {
                $this->view->flash('error', Messages::ERROR_MEMBERS_REQUEST_SENT);
            } else {
                $request = new Member();
                $request->set_user_id($user_id);
                $request->set_group_id($group_id);
                $request->set_status($status);
                if($request->save()) {
                    $this->view->flash('success', Messages::SUCCESS_MEMBERS_REQUEST_SENT);
                } else {
                    $this->view->flash('error', Messages::ERROR_MEMBERS_REQUEST_SENT);
                }
                $this->redirect('');
            }
        } else {
            $this->view->flash('error', Messages::ERROR_DEFAULT);
            $this->redirect('');
        }
    }

    function group_send_request() {
        if(isset($_POST['group_id']) && isset($_POST['user_id'])) {
            $user_id = $_POST['user_id'];
            $group_id = $_POST['group_id'];
            $status = 'group_sent';
            $group = new Group();
            if ($this->model->exists($group_id, $user_id) || !$group->exists($group_id)) {
                return http_response_code(400);
            } else {
                $request = new Member();
                $request->set_user_id($user_id);
                $request->set_group_id($group_id);
                $request->set_status($status);
                if($request->save()) {
                    error_log('se envio');
                    return http_response_code(200);
                } else {
                    return http_response_code(400);
                }
            }
        } else {
            return http_response_code(400);
        }
    }

    function update_request() {
        if(isset($_POST['group_id'])) {
            if($_POST['action'] == 'user_accept') {
                $user_id = $this->user->get_id();
            } else {
                $user_id = $_POST['user_id'];
            }
            $group_id = $_POST['group_id'];
            $status = 'member';
            $role = isset($_POST['role']) ? $_POST['role'] : 'normal';
            $request = new Member();
            $request->set_user_id($user_id);
            $request->set_group_id($group_id);
            $request->set_status($status);
            $request->set_role($role);
            if($request->update()) {
                $this->view->flash('success', Messages::SUCCESS_MEMBERS_ENTERED);
                $res = ['status' => 200];
                http_response_code(200);
            } else {
                $this->view->flash('error', Messages::ERROR_DEFAULT);
                $res = ['status' => 400];
                http_response_code(400);
            }
            echo json_encode($res);
        }
    }


    function delete_request() {
        if(isset($_POST['group_id'])) {
            $userLeave = false;
            if(isset($_POST['leave'])) {
                $user_id = $this->user->get_id();
            }else {
                $user_id = $_POST['user_id'];
            }
            $group_id = $_POST['group_id'];
            if($this->model->delete($group_id, $user_id)) {
                if($userLeave) {
                    $this->view->flash('success', Messages::SUCCESS_MEMBERS_LEFT);
                }
                return http_response_code(200);
            } else {
                return http_response_code(400);
            }
        }
    }


}

?>