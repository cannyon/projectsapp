<?php
require_once 'models/Member.php';
require_once 'models/Group.php';
class Notifications extends SessionController {
    private $user;
    function __construct() {
        parent::__construct();
        $this->user = $this->get_user_session_data();
        $this->view->user = $this->get_user_session_data();
    }

    function render() {
        $invitations = new Group();
        $invitations = $invitations->get_invitations($this->user->get_id());
        if(count($invitations) > 0) {
            $title = '('.count($invitations).') Notificaciones';
        } else {
            $title = 'Notificaciones';
        }
        $this->view->render('users/notifications', $title, 'notifications', 
            [
                'invitations' => $invitations
            ]
        );
    }
}

?>