<?php
require_once 'models/Area.php';
require_once 'models/Member.php';
class Groups extends SessionController{
    private $user;
    function __construct() {
        parent::__construct();
        // Carga el modelo porque tiene otro nombre
        $this->loadModel('group');
        if($this->exists_session()) {
            $this->user = $this->get_user_session_data();
            $this->view->user = $this->get_user_session_data();
        }
    }

    function render() {
        $groups = $this->model->get_all();
        $this->view->render('groups/index', 'Grupos', 'groups', ['groups' => $groups]);
    }

    function view() {
        if(isset($_GET['id'])) {
            $id = $_GET['id'];
            if($this->model->exists($id)) {
                $group = $this->model->get_all_info($id);
                $memberModel = new Member();
                $members = $memberModel->get_members($id);
                // Si inicio sesion
                if(!empty($this->user)) {
                    $canEdit = false;
                    $canEdit = $group->get_creator_id() == $this->user->get_id();
                    $request = new Member();
                    if($request->exists($id, $this->user->get_id())) {
                        $request = $request->get($id, $this->user->get_id());
                        switch($request->get_status()) {
                            case 'user_sent':
                                $request = 2;
                                break;
                            case 'group_sent':
                                $request = 3;
                                break;
                            case 'member':
                                $request = 4;
                                break;
                        }
                    }  else if($group->get_creator_id() == $this->user->get_id()) {
                        // Si el creador soy yo
                        $request = 1;
                    }
                    else {
                        $request = 0;
                    }
    
                    if($canEdit) {
                        $requests = $memberModel->get_pending_requests($id);
                        $freeUsers = $memberModel->get_free_users($this->user->get_id());
                    }
    
                    $this->view->render('groups/view', $group->get_name(), 'groups', 
                        ['group' => $group,
                         'canEdit' => $canEdit,
                         'request' => $request,
                         'requests' => $requests,
                         'members' => $members,
                         'freeUsers' => $freeUsers
                        ]
                    );
                } else {
                    // Ver sin iniciar sesion
                    $this->view->render('groups/view', $group->get_name(), 'groups', ['group' => $group, 'members' => $members]);
                }
            }
        }
    }
    // Render la pagina de crear grupo
    function create() {
        $areaModel = new Area();
        $areas = $areaModel->get_all();
        $this->view->render('groups/create', 'Crear grupo', '', ['areas' => $areas]);
    }

    function edit() {
        if(isset($_GET['id'])) {
            $id = $_GET['id'];
            if($this->model->exists($id)) {
                $group = new Group();
                $group = $group->get($id);
                $areas = new Area();
                $areas = $areas->get_all();
                if($group->get_creator_id() == $this->user->get_id()) {
                    $this->view->render('groups/edit', 'Editar grupo', '', ['group' => $group, 'areas' => $areas]);
                } else {
                    $this->view->flash('error', Messages::ERROR_UNAUTHORIZED);
                    $this->redirect('groups/view?id='.$id);
                }
            } else {
                $this->view->flash('danger', Messages::ERROR_GROUPS_NOT_EXISTS);
                $this->redirect('');
            }
        } else {
            $this->redirect('');
        }
    }

    function new_group() {
        if(isset($_POST['name']) && isset($_POST['area_id'])) {
            $name = $_POST['name'];
            $area_id = $_POST['area_id'];
            if($name == '' || empty($name) || !is_int($area_id)) {
                $this->view->flash('error', Messages::ERROR_EMPTY_FIELDS);
                $this->redirect('groups/create');
            }
            $group = new Group();
            $group->set_name($name);
            $group->set_area_id($area_id);
            $res = $group->save();
            if($res) {
                $this->view->flash('success', Messages::SUCCESS_GROUPS_CREATED);
                $this->redirect('groups');
            }else {
                $this->view->flash('error', Messages::ERROR_DEFAULT);
                $this->redirect('groups/create');
            }
            
        }
    }

    function update_group() {
        if(isset($_POST['name']) && isset($_POST['area_id']) && isset($_POST['id'])) {
            $id = $_POST['id'];
            $name = $_POST['name'];
            $area_id = $_POST['area_id'];
            if($name == '' || empty($name)) {
                $this->view->flash('error', Messages::ERROR_EMPTY_FIELDS);
                $this->redirect('groups/view?id='.$id);
                return;
            }
            $group = new Group();
            $group->set_id($id);
            $group->set_name($name);
            $group->set_area_id($area_id);
            if($group->update()) {
                $this->view->flash('success', Messages::SUCCESS_GROUPS_UPDATED);
                $this->redirect('groups/view?id='.$id);
            }
        } else {
            $this->redirect('');
        }
    }

    function delete_group() {
        if(isset($_POST['id'])) {
            $id = $_POST['id'];
            $res = $this->model->delete($id);
            if($res) {
                $this->view->flash('success', Messages::SUCCESS_GROUPS_DELETED);
                $this->redirect('groups');
            } else {
                $this->view->flash('error', Messages::ERROR_DEFAULT);
                $this->redirect('groups');
            }
        }
    }

    function filter_groups_by_area() {
        $area_id = $_POST['area_id'];
        if(is_numeric($area_id)) {
            $groups = $this->model->get_all_by_area($area_id);
        } else {
            $groups = [];
        }
        echo json_encode($groups);
    }
} 

?>