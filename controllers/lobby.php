<?php

class Lobby extends SessionController{
    private $user;

    function __construct() {
        parent::__construct();
        $this->view->user = $this->get_user_session_data();
    }

    function render() {
        $this->view->render('lobby/index', 'Vestíbulo', '');
    }
}

?>