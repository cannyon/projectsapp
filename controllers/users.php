<?php
require_once 'models/User.php';
class Users extends SessionController
{
    private $user;
    function __construct()
    {
        parent::__construct();
        if ($this->exists_session()) {
            $this->user = $this->get_user_session_data();
            $this->view->user = $this->get_user_session_data();
        }
    }

    function is_it_me($id)
    {
        return $this->user->get_id() == $id;
    }

    function profile()
    {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $userprofile = new User();
            if ($userprofile->exists_by_id($id)) {
                $userprofile = $userprofile->get_profile_info($id);
                $canEdit = false;
                if (isset($this->user) && $this->is_it_me($id)) {
                    $canEdit = true;
                }
                $this->view->render('users/profile', $userprofile->get_full_name() . ' | Perfil', '', ['userprofile' => $userprofile, 'canEdit' => $canEdit]);
            } else {
                $this->view->flash('warning', 'No existe el usuario');
                $this->redirect('');
            }
        } else {
            // Redirige al perfil del usuario loggeado si se va a /users sin especificar id en la url
            if ($this->exists_session()) {
                $this->redirect('users/profile?id=' . $this->user->get_id());
            } else {
                $this->redirect('');
            }
        }
    }

    function update_profile()
    {
        if (isset($_POST['first_name']) && isset($_POST['last_name']) && isset($_POST['email_public'])) {
            $first_name = $_POST['first_name'];
            $last_name = $_POST['last_name'];
            $email_public = $_POST['email_public'];
            // Subir foto de perfil
            if (isset($_FILES['profile_pic'])) {
                $picture = $_FILES['profile_pic'];
                $targetDir = 'public/img/pics/';
                $extension = explode('.', $picture['name']);
                $filename = $extension[sizeof($extension) - 2];
                $ext = $extension[sizeof($extension) - 1];
                $hash = md5(Date('Ymdgi') . $filename) . '.' . $ext;
                $targetFile = $targetDir . $hash;
                $uploadOk = getimagesize($picture['tmp_name']);
                if ($uploadOk) {
                    if (move_uploaded_file($picture['tmp_name'], $targetFile)) {
                        $this->user->set_profile_pic($hash);
                    }
                }
            }
            $this->user->set_first_name($first_name);
            $this->user->set_last_name($last_name);
            $this->user->set_email_public($email_public);
            if ($this->user->get_role() == 'teacher') {
                $dni = isset($_POST['dni'])? $_POST['dni'] : NULL;
                $degree = isset($_POST['degree'])? $_POST['degree'] : NULL;
                $exp_years = isset($_POST['exp_years'])? (int)$_POST['exp_years'] : NULL;
                error_log('Valores teacher: dni: '.$dni.', degree: '.$degree.', exp_years: '.$exp_years);
                $this->user->set_dni($dni);
                $this->user->set_degree($degree);
                $this->user->set_exp_years($exp_years);
            }

            // Si ya tiene foto y la borra
            if ($this->user->get_profile_pic() != 'default.png' && $_POST['deleted_picture'] == '1') {
                $current_pic = 'public/img/pics/' . $this->user->get_profile_pic();
                if (file_exists($current_pic)) {
                    unlink($current_pic);
                    $this->user->set_profile_pic('default.png');
                }
            }
            if ($this->user->update()) {
                $this->view->flash('success', Messages::SUCCESS_USERS_UPDATED);
            } else {
                $this->view->flash('danger', Messages::ERROR_USERS_UPDATED);
            }
        }
        $this->redirect('users/profile?id=' . $this->user->get_id());
    }

    function edit()
    {
        if (isset($this->user)) {
            if (isset($_GET['id']) && $this->is_it_me($_GET['id'])) {
                $id = $_GET['id'];
                $user = new User();
                $user = $user->get_profile_info($id);
                if ($this->user->get_role() == 'teacher') {
                    $this->view->render('users/edit-teacher', 'Editar perfil', '', ['user' => $user]);
                } else {
                    $this->view->render('users/edit', 'Editar perfil', '', ['user' => $user]);
                }
                return;
            }
        }
        $this->view->flash('danger', 'Usuario inválido');
        $this->redirect('');
    }

    function delete_profile_pic()
    {
        if (isset($_POST)) {
            $user = new User();
            $user->set_id($this->user->get_id());
            $current_pic = 'public/img/pics/' . $this->user->get_profile_pic();
            if (file_exists($current_pic)) {
                unlink($current_pic);
                $user->set_profile_pic('default.png');
                return $user->update_profile_pic();
            } else {
                return false;
            }
        }
    }
}
