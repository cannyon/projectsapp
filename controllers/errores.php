<?php

class Errores extends SessionController {
    function __construct() {
        parent::__construct();
        if($this->exists_session()) {
            $this->view->user = $this->get_user_session_data();
        }
        $this->view->render('errores/index', 'Error Not Found');
    }
}

?>