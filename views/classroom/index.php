<div class="container">
    <section>
        <h2>Mis grupos</h2>
        <a class="btn btn-primary" href="<?php echo constant('URL')?>/groups/create">Agregar grupo</a>
        <h3>Creados por mí</h3>
        <?php foreach($this->d['myGroups'] as $group) {?>
            <div class="card">
                <div class="card__header">
                    <h3><?php echo $group->get_name();?></h3>
                </div>
                <div class="card__body">
                    <a href="<?php echo constant('URL').'/groups/view?id='.$group->get_id();?>" class="btn">Leer mas</a>
                </div>
            </div>
        <?php }?>
        <h3>Grupos en los que soy miembro</h3>
        <?php foreach($this->d['groupsIBelongTo'] as $group) {?>
                <div class="card">
                    <div class="card__header">
                        <h3><?php echo $group->get_name();?></h3>
                    </div>
                    <div class="card__body">
                        <a href="<?php echo constant('URL').'/groups/view?id='.$group->get_id();?>" class="btn">Leer mas</a>
                    </div>
                </div>
        <?php }?>
    </section>
    <section>
        <h2>Mis proyectos</h2>
        <a class="btn btn-primary" href="<?php echo constant('URL')?>/researches/create">Agregar trabajo</a>
        <?php foreach($this->d['myWorks'] as $work) {?>
            <div class="card">
                <div class="card__header">
                    <h3><?php echo $work->get_name();?></h3>
                </div>
                <div class="card__body">
                    <a href="<?php echo constant('URL').'/researches/works?id='.$work->get_id();?>" class="btn">Leer mas</a>
                </div>
            </div>
        <?php }?>
    </section>
</div>