<?php 
    $user_edit = $this->d['user_edit'];
    $careers = $this->d['careers'];
?>
<div class="container">
    <h1>Editar usuario</h1>
    <form action="<?php echo constant('URL').'/dashboard/update_user';?>" method="POST">
        <input type="hidden" name="id" value="<?php echo $user_edit->get_id();?>">
        <label for="role">Rol</label>
        <select name="role" id="role">
            <option value="student" <?php if($user_edit->get_role() == 'student') echo 'selected'?>>Estudiante</option>
            <option value="teacher" <?php if($user_edit->get_role() == 'teacher') echo 'selected'?>>Profesor</option>
            <option value="admin" <?php if($user_edit->get_role() == 'admin') echo 'selected'?>>Administrador</option>
        </select>
        <label for="career">Carrera</label>
        <select name="career_id" id="career">
            <option>---</option>
            <?php foreach($careers as $career) {?>
                <option value="<?php echo $career->get_id();?>" <?php if($user_edit->get_career_id() == $career->get_id()) echo 'selected';?>>
                    <?php echo $career->get_name()?>
                </option>
            <?php }?>
        </select>
        <button class="btn" type="submit">Guardar</button>
    </form>
</div>