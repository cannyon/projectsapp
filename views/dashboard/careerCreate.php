<?php $careers = $this->d['careers']; ?>

<div class="container">
    <div class="card card--form">
        <form action="<?php echo constant('URL');?>/dashboard/new_career" method="POST">
            <h1>Crear carrera</h1>
            <div class="form-group">
                <label for="name">Nombre</label>
                <input type="text" id="name" name="name" required>
                <label for="resolution">Resolución</label>
                <input type="text" id="resolution" name="resolution" required>
            </div>
            <button class="btn" type="submit">Crear</button>
        </form>
    </div>
</div>
<div class="container">
    <?php if(!empty($careers)) {?>
    <table>
        <caption>Carreras</caption>
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Resolución</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($careers as $career) {?>
            <tr>
                <td><?php echo $career->get_name();?></td>
                <td><?php echo $career->get_resolution();?></td>
                <td>
                    <a class="btn" href="<?php echo constant('URL').'/dashboard/careers?edit='.$career->get_id()?>">Editar</a>
                    <a class="btn" href="">&times;</a>
                </td>
            </tr>
            <?php }?>
        </tbody>
    </table>
    <?php } else {?>
    <h2>No se han creado carreras</h2>
    <?php }?>
</div>