<?php $area = $this->d['area'];?>
<div class="container">
    <div class="card card--form">
        <form action="<?php echo constant('URL');?>/dashboard/update_area" method="POST">
            <input type="hidden" name="id" value="<?php echo $area->get_id()?>">
            <h1>Editar área</h1>
            <div class="form-group">
                <label for="name">Nombre</label>
                <input type="text" id="name" name="name" value="<?php echo $area->get_name();?>" required>
            </div>
            <button class="btn" type="submit">Guardar</button>
        </form>
    </div>
</div>