<?php $areas = $this->d['areas'];?>

<div class="container">
    <div class="card card--form">
        <form action="<?php echo constant('URL');?>/dashboard/new_area" method="POST">
            <h1>Crear área</h1>
            <div class="form-group">
                <label for="name">Nombre</label>
                <input type="text" id="name" name="name" required>
            </div>
            <button class="btn" type="submit">Crear</button>
        </form>
    </div>
</div>

<div class="container">
    <?php if(!empty($areas)) {?>
    <table>
        <caption>Áreas</caption>
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($areas as $area) {?>
            <tr>
                <td><?php echo $area->get_name();?></td>
                <td>
                    <a class="btn" href="<?php echo constant('URL').'/dashboard/areas?edit='.$area->get_id()?>">Editar</a>
                    <a class="btn" href="">&times;</a>
                </td>
            </tr>
            <?php }?>
        </tbody>
    </table>
    <?php } else {?>
    <h2>No se han creado áreas</h2>
    <?php }?>
</div>