<?php
$users = $this->d['users'];
?>
<h1>Usuarios</h1>
<h2>Estudiantes</h2>
<ul>
<?php foreach($users['students'] as $student) {?>
    <li>
        <a href="<?php echo constant('URL').'/users/profile?id='.$student->get_id();?>">
            <?php echo $student->get_full_name();?>
        </a>
        <a class="btn" href="<?php echo constant('URL').'/dashboard/users?edit='.$student->get_id();?>">
            Editar
        </a>
    </li>
<?php }?>
</ul>
<h2>Profesores</h2>
<ul>
<?php foreach($users['teachers'] as $student) {?>
    <li>
        <a href="<?php echo constant('URL').'/users/profile?id='.$student->get_id();?>">
            <?php echo $student->get_full_name();?>
        </a>
        <a class="btn" href="<?php echo constant('URL').'/dashboard/users?edit='.$student->get_id();?>">
            Editar
        </a>
    </li>
<?php }?>
</ul>
<h2>Administradores</h2>
<ul>
<?php foreach($users['admins'] as $student) {?>
    <li>
        <a href="<?php echo constant('URL').'/users/profile?id='.$student->get_id();?>">
            <?php echo $student->get_full_name();?>
        </a>
        <a class="btn" href="<?php echo constant('URL').'/dashboard/users?edit='.$student->get_id();?>">
            Editar
        </a>
    </li>
<?php }?>
</ul>
