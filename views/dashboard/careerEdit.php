<?php
$career = $this->d['career'];
?>
<div class="container">
    <div class="card card--form">
        <form action="<?php echo constant('URL');?>/dashboard/new_career" method="POST">
            <h1>Editar carrera</h1>
            <div class="form-group">
                <label for="name">Nombre</label>
                <input type="text" id="name" name="name" value="<?php echo $career->get_name();?>" required>
                <label for="resolution">Resolución</label>
                <input type="text" id="resolution" name="resolution" value="<?php echo $career->get_resolution();?>" required>
            </div>
            <button class="btn" type="submit">Guardar</button>
        </form>
    </div>
</div>