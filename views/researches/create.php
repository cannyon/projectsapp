<?php $areas = $this->d['areas'];?>
<section class="container">
  <div class="card">
    <form action="<?php echo constant('URL')?>/researches/new_research" method="POST">
      <h1>Añadir proyecto</h1>
      <div class="form-group">
        <label for="name">Nombre</label>
        <input type="text" id="name" name="name" autocomplete="false" required>
        <label for="startDate">Fecha de inicio</label>
        <input type="date" name="date_start" id="startDate" required>
        <label for="endDate">Fecha de finalización</label>
        <input type="date" name="date_end" id="endDate" required>
        <label for="budget">Presupuesto</label>
        <input type="number" name="budget" id="budget" min="0">
        <label for="area">Área de investigación</label>
        <select name="area_id" id="area">
          <option>---</option>
          <?php foreach($areas as $area) {
            echo '<option value="'.$area->get_id().'">'.$area->get_name().'</option>';
          }?>
        </select>
        <label for="description">Descripción</label>
        <textarea name="description" class="work__description" id="description" autocomplete="false"></textarea>
        <button class="btn" type="submit">Añadir Entrega</button>
      </div>
    </form>
  </div>
</section>