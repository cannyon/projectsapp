<?php
    $work = $this->d['work'];
    $date_start = date('Y-m-d', strtotime($work->get_date_start()));
    $date_end = date('Y-m-d', strtotime($work->get_date_end()));
?>
<section class="container">
  <div class="card">
    <form action="<?php echo constant('URL')?>/researches/update_research" method="POST">
      <h1>Editar proyecto</h1>
      <div class="form-group">
        <input type="hidden" name="id" value="<?php echo $work->get_id();?>">
        <label for="name">Nombre</label>
        <input type="text" id="name" name="name" autocomplete="false" value="<?php echo $work->get_name();?>" required>
        <label for="startDate">Fecha de inicio</label>
        <input type="date" name="date_start" id="startDate" value="<?php echo $date_start;?>" required>
        <label for="endDate">Fecha de finalización</label>
        <input type="date" name="date_end" id="endDate" value="<?php echo $date_end;?>" required>
        <label for="budget">Presupuesto</label>
        <input type="number" name="budget" id="budget" min="0" value="<?php echo $work->get_budget();?>">
        <label for="area">Área de investigación</label>
        <select name="area_id" id="area" data-url="<?php echo constant('URL')?>">
          <option>---</option>
          <?php foreach($this->d['areas'] as $area) {
            echo '<option value="'.$area->get_id().'"'.($work->get_area_id() == $area->get_id() ? 'selected' : '').'>'.$area->get_name().'</option>';
          }?>
        </select>
        <label for="group">Grupo</label>
        <select name="group_id" id="group" data-group_id="<?php echo $work->get_group_id()?>">
          <option>---</option>
          <!-- Aca van los grupos cuando cambio de area -->
        </select>
        <label for="description">Descripción</label>
        <textarea name="description" class="work__description" id="description" autocomplete="false">
          <?php echo $work->get_description();?>
        </textarea>
        <label for="onwork">En trabajo</label>
        <input type="radio" name="state" value="onwork" id="onwork" <?php if($work->get_state() == 'onwork') echo 'checked'?>>
        <label for="stopped">Pausado</label>
        <input type="radio" name="state" value="stopped" id="stopped" <?php if($work->get_state() == 'stopped') echo 'checked'?>>
        <label for="cancelled">Cancelado</label>
        <input type="radio" name="state" value="cancelled" id="cancelled" <?php if($work->get_state() == 'cancelled') echo 'checked'?>>
        <label for="finished">Finalizado</label>
        <input type="radio" name="state" value="finished" id="finished" <?php if($work->get_state() == 'finished') echo 'checked'?>>
        <button class="btn" type="submit">Guardar</button>
      </div>
    </form>
  </div>
</section>

<script src="<?php echo constant('URL')?>/public/js/edit-project.js"></script>