<?php
$work = $this->d['work'];
$states = ['onwork' => 'En trabajo', 'stopped' => 'Pausado', 'Cancelled' => 'Cancelado', 'finished' => 'Finalizado'];
$date_start = date('d/m/Y', strtotime($work->get_date_start()));
$date_end = date('d/m/Y', strtotime($work->get_date_end()));
?>
<div class="container">
  <div class="card">
    <div class="card__body">
      <h1><?php echo $work->get_name();?></h1>
      <p>
        <strong>Profesor a cargo del proyecto: </strong>
        <a href="<?php echo constant('URL').'/users/profile?id='.$work->get_creator_id();?>">
          <?php echo $work->first_name.' '.$work->last_name;?>
        </a>
      </p>
      <p><strong>Equipo a cargo: </strong>
        <?php if($work->get_group_id() != NULL) {?>
          <a href="<?php echo constant('URL').'/groups/view?id='.$work->get_group_id()?>"><?php echo $work->group_name;?></a>
        <?php }else { echo 'Sin asignar'; }?>
      <p><strong>Presupuesto: </strong>$<?php echo number_format($work->get_budget(), 2);?></p>
      <p><strong>Estado: </strong>
        <?php echo $states[$work->get_state()];?>
      </p>
      <p><strong>Área: </strong><?php echo $work->area_name;?></p>
      <p><strong>Fecha de inicio: </strong><?php echo $date_start;?></p>
      <p><strong>Fecha de finalización: </strong><?php echo $date_end;?></p>
      <h4>Descripción</h4>
      <figure>
        <blockquote><?php echo $work->get_description();?></blockquote>
      </figure>
      <?php if($this->d['canEdit']) {?>
      <a class="btn-icon" href="<?php echo constant('URL').'/researches/edit?id='.$work->get_id();?>">
        <i class="material-icons">edit</i>
      </a>
      <?php }?>

      <?php if($this->d['hasUpload']) {?>
        <a href="<?php echo constant('URL').'/public/doc/'.$this->d['upload']->get_url();?>" class="btn-icon" target="_blank">
          <i class="material-icons">local_library</i>
          <?php echo $this->d['upload']->get_filename();?>
        </a>
        <?php if($this->d['canEditUpload']) {?>
          <a href="<?php echo constant('URL').'/researches/upload?id='.$work->get_id();?>">Editar archivo</a>
        <?php }?>
      <?php }else {?>
        <p>Aún no se ha realizado ninguna entrega</p>
        <?php if($this->d['canEditUpload']) {?>
          <a class="btn-icon" href="<?php echo constant('URL').'/researches/upload?id='.$work->get_id();?>"><i class="material-icons">file_upload</i></a>
        <?php }?>
      <?php }?>
    </div>
  </div>
</div>