<div class="container">
    <h1>Todas las investigaciones</h1>
    <?php if(!empty($d['works'])) {?>
        <div class="card-deck">
            <?php foreach($d['works'] as $work) {?>
                <div class="card">
                    <h4 class="card__header"><?php echo $work->get_name() ?></h5>
                        <div class="card__body">
                            <p><strong>Carrera: </strong>
                                <?php echo ($work->get_careerid() != null) ? $work->get_name() : 'Libre'; ?>
                            </p>
                            <p><strong>Asignatura: </strong>
                                {{#if subject}}
                                    {{subject}}
                                {{else}}
                                    Libre
                                {{/if}}
                            </p>
                            <a href="/investigaciones/{{_id}}">Más información</a>
                        </div>
                </div>
            <?php }?>
        </div>
    <?php } else {?>
        <h2>No se encontraron investigaciones</h2>
    <?php }?>
</div>