<?php
$work = $this->d['work'];
$upload = $this->d['upload'];
?>
<div class="container">
    <h1><?php echo $work->get_name();?></h1>
    <h2>Subir archivo del proyecto</h2>
    <p>Suba el resultado de la investigación</p>
    <?php if($upload) { 
        $date_uploaded = date('F j, Y, G:i', strtotime($upload->get_date_uploaded()));
        ?>
        <p>Subida actual:
            <a href="<?php echo constant('URL').'/public/doc/'.$upload->get_url()?>">
                <?php echo $upload->get_filename();?>
            </a>
        </p>
        <p>Subido: <?php echo $date_uploaded;?></p>
    <?php } else {?>
        <p>No se han realizado subidas</p>
    <?php }?>
    <form action="<?php echo constant('URL')?>/researches/upload_file" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $work->get_id();?>" accept="application/pdf, application/msword">
        <label for="pdf" class="btn">
            <i class="material-icons">upload_file</i>
            Subir archivo
        </label>
        <input type="file" name="pdf" id="pdf">
        <button class="btn" type="submit">Guardar</button>
    </form>
</div>