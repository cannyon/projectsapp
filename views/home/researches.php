<div class="container">
  <h1>Todas las investigaciones</h1>
    <?php if(!empty($this->d['works'])) {?>
    <div class="card-deck">
        <?php foreach($this->d['works'] as $work) {?>
            <div class="card">
                <h4 class="card__header"><?php echo $work->get_name()?></h5>
                <div class="card__body">
                    <p><strong>Área: </strong>
                      <?php echo (isset($work->area_name)) ? $work->area_name : 'Libre';?>
                    </p>
                    <a href="<?php echo constant('URL').'/researches/works?id='.$work->get_id()?>">Más información</a>
                </div>
            </div>
        <?php }?>
    </div>
    <?php } else {?>
    <h2>No se encontraron investigaciones</h2>
    <?php }?>
</div>