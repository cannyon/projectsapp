<div class="container">
  <h1><abbr title="Instituto Superior de Formación Técnica &numero;38">ISFT &numero;38</abbr></h1>
  <h2><em>Historia:</em></h2>

  <p>Gracias a las iniciativa de un grupo de personas, surge en el año 1972 la necesidad de la creación del Instituto de
    Formación Técnica Nº 38, destinado a la enseñanza técnica de nivel superior para los habitantes de nuestra zona.</p>

  <p>Por entonces se vislumbró que la estructura interna del sector productivo, había alcanzado un grado de
    heterogeneidad mucho mayor que el que tuviera hasta ese momento, generando en consecuencia, un estancamiento del
    volumen de mano de obra especializado, para las ramas más dinámicas de la industria y el comercio. Con el objeto de
    satisfacer la demanda de ocupaciones que requerían niveles educacionales específicos de alta calificación, se
    canalizaron los objetivos al sector de servicios. También se tuvieron en cuenta la realidad económica y las
    crecientes aspiraciones de las personas del lugar, que deseaban mejorar su nivel técnico-formativo.</p>

  <p>Como consecuencia del deterioro de la situación económica de nuestra zona y alrededores, cada vez fue mayor la
    cantidad de jóvenes egresados de escuelas secundarias que dejaban de emigrar hacia las grandes urbes en busca de una
    Educación Superior, volcándose a las nuevas e interesantes carreras que la región demandaba, asegurando amplios
    campos laborales y futuro a sus egresos.De esta manera, los estudiantes no solo no afectaban el presupuesto familiar
    (viajando, manteniendo viviendas en grandes ciudades) sino también podían contribuir a la economía familiar
    realizando alguna actividad laboral. </p>

  <p>Este cúmulo de circunstancias ayudaron a definir los ejes que permitieron enfocar un nuevo tratamiento de la
    Educación Superior. Atento a lo expuesto, el 28 de noviembre de 1972 el entonces Ministerio de Educación de la
    Provincia de Buenos Aires emitió la resolución Nª 2965/72 que dispuso la creación del establecimiento.</p>

  <p>Este Instituto superior, se caracterizó por la flexibilidad estructural desde que, en el año 1973, comenzó su
    actividad académica con el dictado de las Licenciaturas en Administración de Empresas y en administración de
    personal. En 1979, se reestructura con carreras de Técnicos Superiores (Análisis de Sistemas, Administración de
    Empresas, Seguridad e Higiene Industrial y Mantenimiento Mecánico). A partir de 1982 se inicia el dictado de las
    carreras de Formación Docente, comenzando con la carrera de Asistente Educacional y, en 1988, Magisterio
    Especializado en Educación de Adultos. Para los docentes en actividad desde 1985 se implementaron carreras con
    modalidad “no residentes” (Actualización Docente, Conducción de servicios Educativos, Supervisión de servicios
    educativos, Supervisión de Servicios Educativos y Capacitación Docente Nivel I y Nivel II). Con las mismas
    características se abrió en 1991 la carrera de bibliotecnología (Auxiliar, Escolar y Profesional). Paralelamente
    desde 1985 se inició el distado de carreras regulares como el Profesorado en Psicopedagogía y en 1992 el Profesorado
    de Educación Física. En 1994 se dictó Capacitación Docente nivel III, orientada especialmente a los profesores de la
    casa (egresados universitarios) para mejorar su quehacer pedagógico. En el mismo año se crea la Extensión Ramallo,
    con el dictado de las carreras Técnico Superior en Administración de Empresas y el Profesorado Especializado en
    jardín maternal. El curso de Operador de PC para los internos de la Unidad Penal Nª3 en la Extensión allí creada en
    1991, continúa desarrollándose desde esa fecha.</p>

  <p>A partir de 1997, el Instituto Superior Nª38 volvió a ser exclusivamente técnico y actualmente se dictan las
    carreras de Analista de Sistemas de Información y Analista en Administración de Empresas, en la Sede Central;
    Analista en Administración de Empresas y Operador de PC, en la extensión Ramallo y Operador de PC, en la Extensión
    Unidad Penal Nª3. En 1998 se da apertura a la carrera de Técnico Superior en Seguridad, Higiene y Control Ambiental
    Industrial.
  </p>

  <p>La Institución pretende contar con los mejores recursos técnicos pedagógicos, y para llevar adelante esta
    propuesta, cuenta con profesores de primer Nivel, una tradición académica de consideración, una creciente actividad
    en la capacitación de su personal y un incondicional apoyo de su asociación Cooperadora y del Centro de Estudiantes.
  </p>
</div>