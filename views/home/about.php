<div class="container">
  <h1>Contacto</h1>
  <h2><strong><em>Atención de secretaría: Lunes a viernes, de 17:45 a 22:00hs</em></strong></h2>

  <ul>
    <h3><strong>Sede central San Nicolás:</strong></h3>
    <ul>
      <li>
        <p>Telefono/s:0336-4461110 | 0336-4462857</p>
      </li>
      <li>
        <p>Direccion: Av. Central 1825</p>
      </li>
      <li>
        <p>Email: info@isft38.edu.ar</p>
      </li>
    </ul>
  </ul>


  <ul>
    <h3><strong>Extensión Ramallo (Villa Ramallo)</strong></h3>
    <ul>
      <li>
        <p>Telefono/s: 03407-486443 | 03407-489152</p>
      </li>
      <li>
        <p>Direccion: Dr. Bonfiglio 561</p>
      </li>
      <li>
        <p>Email: ramallo@isft38.edu.ar</p>
      </li>
    </ul>
  </ul>

  <ul>
    <h3><strong>Subsede Conesa</strong></h3>
    <ul>
      <li>
        <p>Telefono: 0336-4492188</p>
      </li>
      <li>
        <p>Direccion: Belgrano 480</p>
      </li>
      <li>
        <p>conesa@isft38.edu.ar</p>
      </li>
    </ul>
  </ul>

  <fieldset>
    Nombre
    <br>
    <input type="text" name="Name">
    <br /><br>

    Apellido
    <br>
    <input type="text" name="Surname">
    <br /><br>

    <!--Sede-->
    <br>
    <label for="Sede">Sede</label>
    <select name="Sede" id="Sede">
      <option value="Sede central San Nicolás - info@isft38.edu.ar">Sede central San Nicolás - info@isft38.edu.ar
      </option>
      <option value="Extensión Ramallo (Villa Ramallo) - ramallo@isft38.edu.ar">Extensión Ramallo (Villa Ramallo) -
        ramallo@isft38.edu.ar</option>
      <option value="Subsede Conesa . conesa@isft38.edu.ar">Subsede Conesa . conesa@isft38.edu.ar</option>
    </select>
    <br /><br>
    Correo electronico:
    <input type="text" name="Email">
    <br /><br>
    Mensaje:
    <input type="text" name="Message">
    <br /><br>
    <input type="submit" name="Send" value="Enviar">
  </fieldset>
</div>