<div class="home">
    <div class="home__hero">
        <img class="home__hero__img" src="public/img/banner.jpg" alt="Hero Image">
    </div>
    <div class="home__hero-overlay"></div>
    <div class="home__data">
        <div class="home__data__items">
            <h1 class="home_title">ISFT &numero; 38</h1>
            <p class="home_name">Bienvenido</p>
        </div>
    </div>
</div>