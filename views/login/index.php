<section class="container">
  <div class="card card--form">
    <form action="<?php echo constant('URL');?>/login/authenticate" method="POST">
      <h1>Iniciar sesión</h1>
      <div class="form-group">
        <label for="username">Nombre de cuenta</label>
        <input type="text" name="username" id="username" spellcheck="false">
        <label for="password">Contraseña</label>
        <input type="password" name="password" id="password">
        <p><small>¿No tiene una cuenta? <a href="<?php echo constant('URL');?>/signup">Cree una.</a></small></p>
        <!-- <p><small>Cree una cuenta de profesor <a href="/signupteacher-auth">Registrar profesor</a></small></p> -->
        <button class="btn" type="submit">Ingresar</button>
      </div>
    </form>
  </div>
</section>