<section class="container">
  <div class="card card--form">
    <form action="<?php echo constant('URL'); ?>/signup/new_user" method="POST">
      <h1>Registrarse</h1>
      <div class="form-group">
        <label for="first_name">Nombre</label>
        <input type="text" name="first_name" id="name" spellcheck="false" autocomplete="false" required>
        <label for="last_name">Apellido</label>
        <input type="text" name="last_name" id="name" spellcheck="false" autocomplete="false" required>
        <label for="username">Nombre de usuario</label>
        <input type="text" name="username" id="username" required>
        <label for="email">Correo electrónico</label>
        <input type="email" name="email" id="email" required>
        <label for="password">Contraseña</label>
        <input type="password" name="password" id="password" required>
        <label for="confirm_password">Confirmar contraseña</label>
        <input type="password" name="confirm_password" id="confirm_password" required>
      </div>
      <div class="form-group">
        <fieldset>
          <legend>Fecha de nacimiento</legend>
          <div class="form__birthday">
            <div class="form-group">
              <label for="monthSelect">Mes</label>
              <select name="month" id="monthSelect" required>
                <option aria-disabled="true" disabled selected="selected"></option>
                <option value="1">Enero</option>
                <option value="2">Febrero</option>
                <option value="3">Marzo</option>
                <option value="4">Abril</option>
                <option value="5">Mayo</option>
                <option value="6">Junio</option>
                <option value="7">Julio</option>
                <option value="8">Agosto</option>
                <option value="9">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
              </select>
            </div>
            <div class="form-group">
              <label for="daySelect">Día</label>
              <select name="day" id="daySelect" required>
                <option aria-disabled="true" disabled selected="selected"></option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
                <option value="31">31</option>
              </select>
            </div>
            <div class="form-group">
              <label for="yearSelect">Año</label>
              <select name="year" id="yearSelect" required>
                <option aria-disabled="true" disabled selected="selected"></option>
              </select>
            </div>
          </div>
        </fieldset>
      </div>
      <div class="form-group">
        <button class="btn" type="submit">Registrar</button>
      </div>
    </form>
  </div>
</section>

<script src="<?php echo constant('URL');?>/public/js/script1.js"></script>