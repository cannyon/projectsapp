<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo constant('URL') ?>/public/css/style.css">
    <link rel="shortcut icon" href="<?php echo constant('URL') ?>/public/img/logo.jpg" type="image/x-icon">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title><?php echo $this->title; ?></title>
</head>
<body>
    <?php require 'views/partials/navbar.php'; ?>
    <?php
    // Si hay mensajes
    if(isset($_SESSION['message'])) {
        $message = $_SESSION['message'];
        ?>
        <div class="alert alert-<?php echo $message['type']?>">
            <?php echo $message['message']?>
            <button>&times;</button>
        </div>
    <?php
    unset($_SESSION['message']); 
    }?>
    <!-- BODY -->
    <?php require $this->body; ?>

    <script src="<?php echo constant('URL');?>/public/js/script.js"></script>
</body>
</html>