<?php
  if(isset($this->user)) { 
    $user = $this->user;
    switch($user->get_role()) {
      case 'student':
        $default_site = 'lobby';
        $default_site_name = 'Lobby';
        break;
      case 'teacher':
        $default_site = 'classroom';
        $default_site_name = 'Salón';
        break;
      case 'admin':
        $default_site = 'dashboard';
        $default_site_name = 'Panel';
        break;
    }
  }
?>
<header>
  <nav class="nav">
    <div class="nav__container">
      <div class="nav__menu" id="nav-menu">
        <ul class="nav__list">
          <li class="nav__item <?php if($this->active == 'home') echo 'nav__item--active'; ?>">
            <a href="<?php echo constant('URL') ;?>" class="nav__link">
              <i class="material-icons nav__icon">home</i>
              <span>Inicio</span>
            </a>
          </li>
          <li class="nav__item <?php if($this->active == 'researches') echo 'nav__item--active'; ?>">
            <a href="<?php echo constant('URL') ;?>/researches" class="nav__link">
              <i class="material-icons nav__icon">library_books</i>
              <span>Investigaciones</span>
            </a>
          </li>
          <li class="nav__item <?php if($this->active == 'groups') echo 'nav__item--active'; ?>">
            <a href="<?php echo constant('URL') ;?>/groups" class="nav__link">
              <i class="material-icons nav__icon">people</i>
              <span>Grupos</span>
            </a>
          </li>
          <li class="nav__item <?php if($this->active == 'institute') echo 'nav__item--active'; ?>">
            <a href="<?php echo constant('URL') ;?>/institute" class="nav__link">
              <i class="material-icons nav__icon">business</i>
              <span>Instituto</span>
            </a>
          </li>
          <li class="nav__item <?php if($this->active == 'about') echo 'nav__item--active'; ?>">
            <a href="<?php echo constant('URL') ;?>/about" class="nav__link">
              <i class="material-icons nav__icon">perm_contact_calendar</i>
              <span>Nosotros</span>
            </a>
          </li>
          <?php if(isset($user)) {?>
          <li class="nav__item <?php if($this->active == $default_site) echo 'nav__item--active';?>">
            <a href="<?php echo constant('URL').'/'.$default_site;?>" class="nav__link">
              <?php echo $default_site_name;?>
            </a>
          </li>
          <?php }?>

        </ul>
      </div>
      <?php if(isset($user)) {?>
      <div class="nav__item <?php if($this->active == 'notifications') echo 'nav__item--active'; ?>">
        <a class="nav__link" href="<?php echo constant('URL')?>/notifications">
          <i class="material-icons">notifications</i>
          Notificaciones
        </a>
      </div>
      <div class="nav__item">
        <span class="nav__link nav__link--user" id="btnProfile">
          <img src="<?php echo constant('URL').'/public/img/pics/'.$user->get_profile_pic();?>" alt="Foto de perfil" class="profile_pic--list">
          <?php echo $user->get_full_name();?> &#9660;
        </span>
        <div id="profileMenu" class="profile__menu">
          <a href="<?php echo constant('URL').'/users/profile?id='.$user->get_id();?>" class="nav__link">
            <i class="material-icons nav__icon">person</i>
            <span>Perfil</span>
          </a>
          <a href="<?php echo constant('URL'); ?>/login/logout" class="nav__link">
            <i class="material-icons nav__icon">exit_to_app</i>
            <span>Cerrar sesión</span>
          </a>
        </div>
      </div>
      <?php } else {?>
      <a href="<?php echo constant('URL'); ?>/login" class="btn btn-login">Iniciar sesión</a>
      <?php }?>
      <div class="nav__toggle" id="nav-toggle">
        <span>&#9776;</span>
      </div>
    </div>
  </nav>
</header>