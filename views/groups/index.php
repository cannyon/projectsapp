<div class="container">
    <h1>Grupos de investigación</h1>
    <?php if(!empty($this->d['groups'])) {?>
        <div class="card-deck">
            <?php foreach($this->d['groups'] as $group) {?>
                <div class="card">
                    <h4 class="card__header"><?php echo $group->get_name();?></h4>
                    <div class="card__body">
                        <p>Miembros: <?php echo ($group->members)?></p>
                        <p>Área: <?php echo $group->area_name;?></p>
                        <a href="<?php echo constant('URL').'/groups/view?id='.$group->get_id();?>">Más información</a>
                    </div>
                </div>
            <?php }?>
        </div>
    <?php }else {?>
        <h2>No se encontraron grupos</h2>
    <?php }?>

</div>