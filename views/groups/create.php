<?php
$areas = $this->d['areas'];
?>
<div class="container">
  <div class="card card--form">
    <form action="<?php echo constant('URL');?>/groups/new_group" method="POST">
    <h1>Crear grupo</h1>
      <div id="sectionCreateGroup" class="form-group">
        <label for="name">Nombre</label>
        <input type="text" name="name" id="name" autocomplete="false" required>
        <label for="area">Área</label>
        <select name="area_id" id="area" required>
          <option value="null">---</option>
          <?php foreach($areas as $area) {?>
          <option value="<?php echo $area->get_id();?>"><?php echo $area->get_name();?></option>
          <?php }?>
        </select>
        <button class="btn" type="submit">Crear</button>
      </div>
    </form>
  </div>
</div>