<?php
$group = $this->d['group'];
?>
<section class="group" id="group" data-url="<?php echo constant('URL')?>" data-group_id="<?php echo $group->get_id();?>">
  <h1 class="group__title">Grupo</h1>
  <div>
    <h2 class="group__group-title"><?php echo $group->get_name(); ?></h2>
    <p class="group__subtitle"><small><?php echo $group->area_name; ?></small></p>
  </div>
  <div class="group__about">
    <h3 class="group__about-title">Acerca de <?php echo $group->get_name(); ?></h3>
    <p><strong>Creador: </strong><a href="<?php echo constant('URL') . '/users/profile?id=' . $group->get_creator_id(); ?>"><?php echo $group->creator_name; ?></a></p>
    <p><strong>Se creó el: </strong><?php echo $group->get_created_at(); ?></p>
    <p><strong>Área de investigación: </strong><?php echo $group->area_name ?></p>
    <?php if (!empty($this->user) && $this->d['canEdit']) { ?>
      <a href="<?php echo constant('URL') . '/groups/edit?id=' . $group->get_id(); ?>" class="btn">Editar</a>
      <ul id="receivedRequestsList">
        <?php foreach($this->d['requests'] as $req) {?>
          <li id="user-data" data-user_id="<?php echo $req->get_user_id()?>">
            <a href="<?php echo constant('URL').'/users/profile?id='.$req->get_user_id()?>">
              <img src="<?php echo constant('URL').'/public/img/pics/'.$req->profile_pic?>" class="profile_pic--list" alt="Foto de perfil">
              <?php echo $req->user_name?>
            </a>
            <button id="btnAcceptRequest" type="button" class="btn">
              Aceptar
            </button>
            <button id="btnRejectRequest" type="button" class="btn">
              Rechazar
            </button>
          </li>
        <?php }?>
      </ul>
    <?php }?>

    <?php if(!empty($this->user)) {?>
      <?php if ($this->d['request'] == 0) { ?>
        <button type="button" id="btnSendRequest" class="btn">
          Enviar solicitud
        </button>
      <?php }
      if ($this->d['request'] == 2) { ?>
        <button type="button" id="btnCancelRequest" class="btn">
          Cancelar solicitud
        </button>
      <?php }
      if ($this->d['request'] == 3) { ?>
        <div id="myRequests">
          <button id="btnAcceptRequest" class="btn" type="button">Aceptar solicitud</button>
          <button id="btnRejectRequest" class="btn" type="button">Rechazar solicitud</button>
        </div>
      <?php }
      if ($this->d['request'] == 4) { ?>
        <button id="btnLeave" class="btn" type="button">Abandonar grupo</button>
      <?php } ?>
    <?php }?>
  </div>
</section>

<section>
    <div class="card">
      <div class="card__body">
        <h3>Miembros</h3>
        <?php if(!empty($this->d['members'])) {?>
          <ul>
            <?php foreach($this->d['members'] as $member) {?>
              <li>
                <a href="<?php echo constant('URL').'/users/profile?id='.$member->get_id();?>">
                  <img src="<?php echo constant('URL').'/public/img/pics/'.$member->get_profile_pic()?>" class="profile_pic--list" alt="Foto de perfil">
                  <?php echo $member->get_full_name();?>
                </a>
              </li>
            <?php }?>
          </ul>
        <?php }else {?>
          No hay miembros
        <?php }?>
      </div>
    </div>
</section>

<?php if(!empty($this->user) && $this->d['canEdit']) {?>
<section>
    <div class="card">
        <div class="card__body">
            <h3>Usuarios libres</h3>
            <ul id="freeUsersList">
              <?php foreach($this->d['freeUsers'] as $fu) {?>
                <li data-user_id="<?php echo $fu->get_id();?>">
                  <a href="<?php echo constant('URL').'/users/profile?id='.$fu->get_id()?>">
                    <img src="<?php echo constant('URL').'/public/img/pics/'.$fu->get_profile_pic();?>" class="profile_pic--list" alt="Foto de perfil">
                    <?php echo $fu->get_full_name();?>
                  </a>
                  <?php if($fu->status == 'group_sent' && $fu->group_id == $group->get_id()) {?>
                    <button id="btnCancel" class="btn">Cancelar solicitud</button>
                  <?php }else {?>
                    <button id="btnInvite" class="btn">Enviar solicitud</button>
                  <?php }?>
                </li>
              <?php }?>
            </ul>
        </div>
    </div>
</section>
<?php }?>

<script src="<?php echo constant('URL')?>/public/js/requests.js"></script>