<?php
$areas = $this->d['areas'];
$group = $this->d['group'];
?>
<div class="container">
  <div class="card card--form">
    <form action="<?php echo constant('URL');?>/groups/update_group" method="POST">
    <h1>Editar grupo</h1>
      <div id="sectionCreateGroup" class="form-group">
        <input type="hidden" name="id" value="<?php echo $group->get_id();?>">
        <label for="name">Nombre</label>
        <input type="text" name="name" id="name" autocomplete="false" value="<?php echo $group->get_name();?>" required>
        <label for="area">Área</label>
        <select name="area_id" id="area" required>
          <option value="null">---</option>
          <?php foreach($areas as $area) {?>
          <option value="<?php echo $area->get_id();?>" <?php if($area->get_id() == $group->get_area_id()) echo 'selected'?>>
            <?php echo $area->get_name();?>
          </option>
          <?php }?>
        </select>
        <button class="btn" type="submit">Crear</button>
      </div>
    </form>
  </div>
</div>