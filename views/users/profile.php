<?php
$userprofile = $this->d['userprofile'];
$registered_at = date('d/m/Y', strtotime($userprofile->get_registered_at()));
$birthday = date('d/m/Y', strtotime($userprofile->get_birthday()));
$hasCareer = $userprofile->get_career_id() != NULL;
$roles = ['student' => 'Estudiante', 'teacher' => 'Profesor', 'admin' => 'Administrador'];
?>
<section class="container">
    <h1>Perfil de <?php echo $userprofile->get_full_name();?></h1>
    <img src="<?php echo constant('URL').'/public/img/pics/'.$userprofile->get_profile_pic();?>" class="profile_pic" alt="Foto de perfil">
    
    <h4>Información</h4>
    <p><strong>Carrera:</strong>
        <?php echo $hasCareer ? $userprofile->career_name : 'No tiene carrera';?>
    </p>
    <?php if($userprofile->get_email_public() == 1) {?><p><strong>Correo electrónico:</strong> <?php echo $userprofile->get_email();?></p><?php }?>
    <p><strong>Fecha de nacimiento:</strong> <?php echo $birthday;?></p>
    <p><strong>Se registró el: </strong><?php echo $registered_at;?></p>
    <p><strong>Rol: </strong><?php echo $roles[$userprofile->get_role()];?></p>
    <!-- Info de profesor -->
    <?php if($userprofile->get_role() == 'teacher'){?>
        <h4>Información de profesor</h4>
        <p><strong>DNI:</strong> <?php echo $userprofile->get_dni();?></p>
        <p><strong>Título:</strong> <?php echo $userprofile->get_degree();?></p>
        <p><strong>Años de experiencia:</strong> <?php echo $userprofile->get_exp_years();?></p>
    <?php }?>
    <hr>
    <?php if($this->d['canEdit']){?>
    <a href="<?php echo constant('URL').'/users/edit?id='.$userprofile->get_id();?>" class="btn">Editar</a>
    <?php }?>
</section>