<?php $user = $this->d['user']; ?>
<span hidden id="urlHolder" data-url="<?php echo constant('URL') ?>"></span>
<div class="container">
  <h1>Editar perfil</h1>
  <div class="card">
    <div class="card-body">
      <form
        action="<?php echo constant('URL') ?>/users/update_profile"
        method="POST"
        autocomplete="off"
        enctype="multipart/form-data">
        <div class="form__group" id="divProfilePic">
          <label for="profilePicFile" class="profile_pic">
            <div id="profile_pic" class="profile_pic--edit" style="background-image: url('<?php echo constant('URL') . '/public/img/pics/' . $user->get_profile_pic(); ?>')">
              <span class="material-icons">photo_camera</span>
              <span>Cambiar foto</span>
            </div>
            <input type="hidden" id="deleted_picture" name="deleted_picture" value="0">
            <input
              type="file"
              name="profile_pic"
              id="profilePicFile"
              accept="image/png, image/gif, image/jpeg">
          </label>
          <?php if ($user->get_profile_pic() != 'default.png') { ?>
            <button id="btnDeleteProfilePic" type="button" class="btn">
              Eliminar foto
              <span class="material-icons">delete</span>
            </button>
          <?php } ?>
        </div>

        <div class="form__group">
          <label for="name">Nombre</label>
          <input type="text" name="first_name" required value="<?php echo $user->get_first_name(); ?>">
        </div>
        <div class="form__group">
          <label for="last_name">Apellido</label>
          <input type="text" name="last_name" required value="<?php echo $user->get_last_name(); ?>">
        </div>
        <div class="form__group">
          <h4>Mostrar correo electrónico</h4>
          <label for="public">Público</label>
          <input type="radio" name="email_public" id="public" value="1" <?php if ($user->get_email_public()) echo 'checked' ?>>
          <label for="public">Privado</label>
          <input type="radio" name="email_public" id="public" value="0" <?php if (!$user->get_email_public()) echo 'checked' ?>>
        </div>
        <div class="form__group">
          <label for="dni">DNI</label>
          <input type="text" name="dni" id="dni" pattern="[0-9]{8}" value="<?php echo $user->get_dni(); ?>">
        </div>
        <div class="form__group">
          <label for="exp">Años de experiencia en investigación</label>
          <input type="number" inputmode="numeric" name="exp_years" id="exp" value="<?php echo $user->get_exp_years(); ?>">
        </div>
        <div class="form__group">
          <label for="degree">Título superior</label>
          <input type="text" name="degree" id="degree" value="<?php echo $user->get_degree(); ?>">
        </div>
        <button type="submit" class="btn btn-primary">Guardar cambios</button>
        <a href="<?php echo constant('URL').'/users/profile?id='.$user->get_id()?>" class="btn">Cancelar</a>
      </form>
    </div>
  </div>
</div>

<script src="<?php echo constant('URL') ?>/public/js/script1.js"></script>