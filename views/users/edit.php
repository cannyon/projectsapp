<?php $user = $this->d['user'];?>
<div class="container">
    <h1>Editar perfil</h1>
    <div class="card">
        <div class="card-body">
            <form action="<?php echo constant('URL')?>/users/update_profile" method="POST" autocomplete="off" enctype="multipart/form-data">
                <label for="profile_pic">Foto de perfil</label>
                <?php if($user->get_profile_pic() != NULL) {?>
                    <img src="<?php echo constant('URL').'/public/img/pics/'.$user->get_profile_pic();?>" class="profile_pic" alt="Foto de perfil">
                <?php }?>
                <input type="file" name="profile_pic" id="profile_pic">
                <label for="name">Nombre</label>
                <input type="text" name="first_name" required value="<?php echo $user->get_first_name();?>">
                <label for="last_name">Apellido</label>
                <input type="text" name="last_name" required value="<?php echo $user->get_last_name();?>">
                <label for="email_public">Mostrar correo electrónico</label>
                <select name="email_public" id="email_public">
                    <option value="1" <?php if($user->get_email_public() == 1) echo 'selected';?>>Público</option>
                    <option value="0" <?php if($user->get_email_public() == 0) echo 'selected';?>>Privado</option>
                </select>
                <button type="submit" class="btn btn-primary">Guardar cambios</button>
            </form>
        </div>
    </div>

</div>