<div class="container">
    <h1>Invitaciones a grupos</h1>
    <ul>
        <?php foreach($this->d['invitations'] as $invitation) {?>
            <li>
                <a href="<?php echo constant('URL').'/groups/view?id='.$invitation->get_id()?>">
                    <?php echo $invitation->get_name();?>
                </a>
            </li>
        <?php }?>
    </ul>
</div>